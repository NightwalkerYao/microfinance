<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\CompteController;
use App\Http\Controllers\AgenceController;
use App\Http\Controllers\OperationController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ClientHomeController;
use App\Http\Controllers\DashboardController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group([
  'middleware' => 'auth',
  'prefix' => '/app',
], function(){
  Route::get('/', [DashboardController::class, 'home'])->name("dashboard");
  // Pages client
  Route::group([
    'prefix' => '/client',
    'middleware' => 'has.role:client',
  ], function(){
    Route::get('/comptes', [ClientHomeController::class, 'comptes'])->name("list-comptes-client");
    Route::get('/operations', [ClientHomeController::class, 'operations'])->name("list-operations-client");

    // Route::group(['middleware' => "has.role:gerant|gerant_adjoint"], function(){
    //   Route::get('/add', [CompteController::class, 'create'])->name("create-compte");
    //   Route::post('/add', [CompteController::class, 'store']);
    //   Route::get('/{compte}', [CompteController::class, 'show'])->name("show-compte");
    //   Route::get('/{compte}/edit', [CompteController::class, 'edit'])->name("edit-compte");
    //   Route::post('/{compte}/edit', [CompteController::class, 'update']);
    //   Route::post('/{compte}/delete', [CompteController::class, 'destroy'])->name("delete-compte");
    //   Route::get('/clients/json', [CompteController::class, 'jsonList'])->name("json-list-comptes");
    // });
  });
  // Clients
  Route::group([
    'prefix' => '/utilisateurs',
    'middleware' => 'has.role:gerant',
  ], function(){
    Route::get('/', [UserController::class, 'index'])->name("list-users");
    Route::get('/add', [UserController::class, 'create'])->name("create-user");
    Route::post('/add', [UserController::class, 'store']);
    Route::get('/{user}', [UserController::class, 'show'])->name("show-user");
    Route::get('/{user}/edit', [UserController::class, 'edit'])->name("edit-user");
    Route::post('/{user}/edit', [UserController::class, 'update']);
    Route::post('/{user}/delete', [UserController::class, 'destroy'])->name("delete-user");
  });
  // Clients
  Route::group([
    'prefix' => '/clients',
    'middleware' => 'has.role:gerant|gerant_adjoint',
  ], function(){
    Route::get('/', [ClientController::class, 'index'])->name("list-clients");
    Route::get('/add', [ClientController::class, 'create'])->name("create-client");
    Route::post('/add', [ClientController::class, 'store']);
    Route::get('/{client}', [ClientController::class, 'show'])->name("show-client");
    Route::get('/{client}/edit', [ClientController::class, 'edit'])->name("edit-client");
    Route::post('/{client}/edit', [ClientController::class, 'update']);
    Route::post('/{client}/delete', [ClientController::class, 'destroy'])->name("delete-client");
  });
  // Comptes
  Route::group(['prefix' => '/comptes'], function(){
    Route::get('/', [CompteController::class, 'index'])->name("list-comptes")->middleware("has.role:gerant|gerant_adjoint|caissiere");
    Route::group(['middleware' => "has.role:gerant|gerant_adjoint"], function(){
      Route::get('/add', [CompteController::class, 'create'])->name("create-compte");
      Route::post('/agios', [CompteController::class, 'agios'])->name("agios");
      Route::post('/add', [CompteController::class, 'store']);
      Route::get('/{compte}', [CompteController::class, 'show'])->name("show-compte");
      Route::get('/{compte}/edit', [CompteController::class, 'edit'])->name("edit-compte");
      Route::post('/{compte}/edit', [CompteController::class, 'update']);
      Route::post('/{compte}/delete', [CompteController::class, 'destroy'])->name("delete-compte");
      Route::post('/{compte}/freeze', [CompteController::class, 'freeze'])->name("freeze-compte");
      Route::get('/clients/json', [CompteController::class, 'jsonList'])->name("json-list-comptes");
    });
  });
  // Agences
  Route::group([
    'prefix' => '/agences',
    'middleware' => 'has.role:gerant',
  ], function(){
    Route::get('/', [AgenceController::class, 'index'])->name("list-agences");
    Route::get('/add', [AgenceController::class, 'create'])->name("create-agence");
    Route::post('/add', [AgenceController::class, 'store']);
    Route::get('/{agence}', [AgenceController::class, 'show'])->name("show-agence");
    Route::get('/{agence}/edit', [AgenceController::class, 'edit'])->name("edit-agence");
    Route::post('/{agence}/edit', [AgenceController::class, 'update']);
    Route::post('/{agence}/delete', [AgenceController::class, 'destroy'])->name("delete-agence");
  });
  // Operations
  Route::group(['prefix' => '/operations'], function(){
    Route::group(['middleware' => "has.role:gerant|gerant_adjoint|caissiere"], function(){
      Route::get('/', [OperationController::class, 'index'])->name('list-operations');
      Route::get('/{operation}', [OperationController::class, 'show'])->name('show-operation');
    });
    Route::group(['middleware' => 'has.role:caissiere'], function(){
      Route::get('/comptes/json', [OperationController::class, 'jsonComptes'])->name("json-comptes");
      // Depots
      Route::group(['prefix' => '/depots'], function(){
        // Route::get('/', [OperationController::class, 'listDeposits'])->name('list-depots');
        Route::get('/make', [OperationController::class, 'deposit'])->name('make-depot');
        Route::post('/make', [OperationController::class, 'storeDeposit']);
      });
      // Retraits
      Route::group(['prefix' => '/retraits'], function(){
        // Route::get('/', [OperationController::class, 'listWds'])->name('list-retraits');
        Route::get('/make', [OperationController::class, 'withdraw'])->name('make-retrait');
        Route::post('/make', [OperationController::class, 'storeWithdraw']);
      });
      // Transfert
      Route::group(['prefix' => '/transferts'], function(){
        // Route::get('/', [OperationController::class, 'listTransfers'])->name('list-transferts');
        Route::get('/make', [OperationController::class, 'transfer'])->name('make-transfert');
        Route::post('/make', [OperationController::class, 'storeTransfer']);
      });
    });

  });
});





require __DIR__.'/auth.php';
