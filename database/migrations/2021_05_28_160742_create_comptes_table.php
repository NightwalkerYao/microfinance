<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComptesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comptes', function (Blueprint $table) {
            $table->id();
            $table->string("numero");
            $table->datetime("date_ouverture");
            $table->string("visa_ouverture");
            $table->datetime("date_fermeture")->nullable();
            $table->string("visa_fermeture")->nullable();
            $table->string("observation")->nullable();
            $table->double("solde")->default(0.0);
            $table->foreignId("client_id")->constrained()->onDelete("cascade");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comptes');
    }
}
