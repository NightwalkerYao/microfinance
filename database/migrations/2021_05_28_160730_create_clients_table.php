<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->id();
            $table->string("cni")->nullable();
            $table->string("nom");
            $table->string("prenom");
            $table->date("date_naissance");
            $table->string("profession");
            $table->string("telephone");
            $table->string("photo")->nullable();
            $table->string("signature")->nullable();
            $table->foreignId("sexe_id")->constrained()->onDelete("cascade");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
