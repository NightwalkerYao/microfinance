<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOperationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('operations', function (Blueprint $table) {
            $table->id();
            $table->string("numero");
            $table->datetime("date");
            $table->double("montant");
            $table->string("visa")->nullable();
            $table->string("observation")->nullable();
            $table->foreignId("agence_id")->constrained()->onDelete("cascade");
            $table->foreignId("mode_operation_id")->constrained()->onDelete("cascade");
            $table->foreignId("type_operation_id")->constrained()->onDelete("cascade");
            $table->foreignId("compte_id")->constrained()->onDelete("cascade");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('operations');
    }
}
