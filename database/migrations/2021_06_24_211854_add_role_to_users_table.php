<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRoleToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('role')->nullable()->after('password')->default('caissiere');
            $table->foreignId('client_id')->nullable()->constrained()->onDelete('cascade')->before('created_at');
            $table->boolean('active')->default(true)->before("created_at");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('role');
            $table->dropColumn('active');
            $table->dropForeign(["client_id"]);
            $table->dropColumn("client_id");
        });
    }
}
