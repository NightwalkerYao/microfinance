<?php

namespace Database\Factories;

use App\Models\Client;
use Illuminate\Database\Eloquent\Factories\Factory;
use Intervention\Image\ImageManagerStatic as Image;

class ClientFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Client::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
      $users = json_decode(file_get_contents(database_path("seeders/users.json")), true);
      $u1 = $users['results'][mt_rand(0, 199)];
      $pp1 = $u1['picture']['large'];
      $path = storage_path("app/public/uploads/");
      if(!is_dir($path))
        @mkdir($path, 0755, true);
      $p1 = md5($u1['email']).".jpg";
      Image::make($pp1)->save($path.$p1);

        return [
          "cni" => str_replace(' ', '', $this->faker->vat()),
          "nom" => $this->faker->firstName(),
          "prenom" => $this->faker->lastName(),
          "date_naissance" => $this->faker->date("Y-m-d", date("Y-m-d", strtotime("-16 years"))),
          "profession" => $this->faker->jobTitle(),
          "telephone" => $this->faker->mobileNumber(),
          "photo" => "uploads/".$p1,
          "signature" => "",
          "sexe_id" => mt_rand(1,2),
        ];
    }
}
