<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\TypeOperation;

class TypesOperationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(!(TypeOperation::count())) {
          TypeOperation::insert([
            ['libelle' => "Dépôt"],
            ['libelle' => "Retrait"],
            ['libelle' => "Transfert"],
          ]);
        }
    }
}
