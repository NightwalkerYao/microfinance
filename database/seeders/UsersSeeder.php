<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Intervention\Image\ImageManagerStatic as Image;
use Hash;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $users = json_decode(file_get_contents(database_path("seeders/users.json")), true);

      if(!User::count()) {
        $u1 = $users['results'][mt_rand(0, 199)];
        $pp1 = $u1['picture']['large'];
        $path = storage_path("app/public/photos/");
        if(!is_dir($path))
          @mkdir($path, 0755, true);
        $p1 = md5($u1['email']).".jpg";
        Image::make($pp1)->save($path.$p1);

        User::create([
          'name' => "Nightwalker Yao",
          'matricule' => "NW00007",
          'photo' => $p1,
          'email' => "huntr@pm.me",
          'password' => Hash::make(7777777),
          'role' => "gerant",
          'active' => true,
        ]);

        $u2 = $users['results'][mt_rand(0, 199)];
        $pp2 = $u1['picture']['large'];
        $p2 = md5($u2['email']).".jpg";
        Image::make($pp2)->save($path.$p2);

        User::create([
          'name' => "John Smith",
          'matricule' => "JSM000056",
          'photo' => $p2,
          'email' => "john@smith.com",
          'password' => Hash::make("jsmith"),
          'role' => "gerant_adjoint",
          'active' => true,
        ]);
      }

      $i = 1;
      do {
        $u = $users['results'][mt_rand(0, 199)];
        $pp = $u['picture']['large'];
        $path = storage_path("app/public/photos/");
        if(!is_dir($path))
          @mkdir($path, 0755, true);
        $p = md5($u['email']).".jpg";
        Image::make($pp)->save($path.$p);
        $user = [
          'name' => $u['name']['first'].' '.$u['name']['last'],
          'matricule' => /*$u['id']['value'] ?? */mt_rand(1000000, 99999999).(range("A", "Z")[mt_rand(0,25)]),
          'photo' => $p,
          'email' => $u['email'],
          'password' => Hash::make(123456),
        ];
        if(!empty($user['name']) && !(User::where('email', $user['email'])->exists())) {
          $user = User::create($user);
          echo "User $i/20 : ".$user->name.", [".$user->matricule." : 123456] created.\n";
          $i++;
        }
      } while ($i <= 20);
    }
}
