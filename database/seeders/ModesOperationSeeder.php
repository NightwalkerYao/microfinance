<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ModeOperation;

class ModesOperationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      if(!(ModeOperation::count())) {
        ModeOperation::insert([
          ['libelle' => "Espèce"],
          ['libelle' => "Chèque"],
          ['libelle' => "Transfert d'argent"],
        ]);
      }
    }
}
