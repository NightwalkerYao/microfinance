<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Client;

class ClientUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      foreach (Client::all() as $client) {
        copy(storage_path("app/public/".$client->photo), storage_path("app/public/photos/".basename($client->photo)));
        User::create([
          'name' => $client->nom_complet,
          'matricule' => mt_rand(1000000, 99999999).(range("A", "Z")[mt_rand(0,25)]),
          'photo' => basename($client->photo),
          'email' => \Str::slug($client->nom_complet, '.').$client->id."@microfinance.com",
          'password' => \Hash::make(123456),
          'role' => "client",
          'active' => true,
          'client_id' => $client->id,
        ]);
      }
    }
}
