<div class="header-menu-wrapper header-menu-wrapper-left" id="kt_header_menu_wrapper">
  <div class="container">
    <!--begin::Header Menu-->
    <div id="kt_header_menu" class="header-menu header-menu-left header-menu-mobile header-menu-layout-default header-menu-root-arrow">
      <!--begin::Header Nav-->
      <ul class="menu-nav">
        <li class="menu-item {{ preg_match('/^app\/$/iD', request()->path()) ? 'menu-item-open menu-item-here' : '' }} menu-item-submenu menu-item-rel" aria-haspopup="true">
          <a href="{{ route('dashboard') }}" class="menu-link">
            <span class="menu-text">Accueil</span>
          </a>
        </li>

        <li class="menu-item menu-item-submenu menu-item-rel {{ preg_match('/^app\/client\/comptes/iD', request()->path()) ? 'menu-item-open menu-item-here' : '' }}">
          <a href="{{ route('list-comptes-client') }}" class="menu-link">
            <span class="menu-text">Mes Comptes</span>
            <span class="menu-desc"></span>
          </a>
        </li>

        <li class="menu-item menu-item-submenu menu-item-rel {{ preg_match('/^app\/client\/operations/iD', request()->path()) ? 'menu-item-open menu-item-here' : '' }}">
          <a href="{{ route('list-operations-client') }}" class="menu-link">
            <span class="menu-text">Opérations</span>
            <span class="menu-desc"></span>
          </a>
        </li>

        {{-- <li class="menu-item menu-item-submenu menu-item-rel">
          <a href="#" class="menu-link">
            <span class="menu-text">Relévé de compte</span>
            <span class="menu-desc"></span>
          </a>
        </li> --}}
      </ul>
      <!--end::Header Nav-->
    </div>
    <!--end::Header Menu-->
  </div>
</div>
