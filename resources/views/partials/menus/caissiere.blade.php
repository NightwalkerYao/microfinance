<div class="header-menu-wrapper header-menu-wrapper-left" id="kt_header_menu_wrapper">
  <div class="container">
    <!--begin::Header Menu-->
    <div id="kt_header_menu" class="header-menu header-menu-left header-menu-mobile header-menu-layout-default header-menu-root-arrow">
      <!--begin::Header Nav-->
      <ul class="menu-nav">
        <li class="menu-item {{ preg_match('/^app\/$/iD', request()->path()) ? 'menu-item-open menu-item-here' : '' }} menu-item-submenu menu-item-rel" aria-haspopup="true">
          <a href="{{ route('dashboard') }}" class="menu-link">
            <span class="menu-text">Accueil</span>
          </a>
        </li>

        <li class="menu-item menu-item-submenu menu-item-rel {{ preg_match('/^app\/comptes/iD', request()->path()) ? 'menu-item-open menu-item-here' : '' }}" aria-haspopup="true">
          <a href="{{ route('list-comptes') }}" class="menu-link">
            <span class="menu-text">Comptes</span>
            <span class="menu-desc"></span>
          </a>
        </li>
        <li class="menu-item menu-item-submenu menu-item-rel {{ preg_match('/^app\/operations\/depots/iD', request()->path()) ? 'menu-item-open menu-item-here' : '' }}" aria-haspopup="true">
          <a href="{{ route('make-depot') }}" class="menu-link">
            <span class="menu-text">Dépôt</span>
            <span class="menu-desc"></span>
          </a>
        </li>
        <li class="menu-item menu-item-submenu menu-item-rel {{ preg_match('/^app\/operations\/retraits/iD', request()->path()) ? 'menu-item-open menu-item-here' : '' }}" aria-haspopup="true">
          <a href="{{ route('make-retrait') }}" class="menu-link">
            <span class="menu-text">Retrait</span>
            <span class="menu-desc"></span>
          </a>
        </li>
        <li class="menu-item menu-item-submenu menu-item-rel {{ preg_match('/^app\/operations\/transferts/iD', request()->path()) ? 'menu-item-open menu-item-here' : '' }}" aria-haspopup="true">
          <a href="{{ route('make-transfert') }}" class="menu-link">
            <span class="menu-text">Transfert CàC</span>
            <span class="menu-desc"></span>
          </a>
        </li>
        <li class="menu-item menu-item-submenu menu-item-rel {{ preg_match('/^app\/operations$/iD', request()->path()) ? 'menu-item-open menu-item-here' : '' }}" aria-haspopup="true">
          <a href="{{ route('list-operations') }}" class="menu-link">
            <span class="menu-text">Historique des Opérations</span>
            <span class="menu-desc"></span>
          </a>
        </li>
      </ul>
      <!--end::Header Nav-->
    </div>
    <!--end::Header Menu-->
  </div>
</div>
