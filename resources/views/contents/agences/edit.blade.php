@extends('layouts.dashboard')

@section('title')
  {{ $agence->libelle }}
@endsection

@section('content')
  <div class="d-flex flex-row flex-column-fluid container">
    <!--begin::Content Wrapper-->
    <div class="main d-flex flex-column flex-row-fluid">
      <!--begin::Subheader-->
      <div class="subheader py-2 py-lg-6" id="kt_subheader">
        <div class="w-100 d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
          <!--begin::Info-->
          <div class="d-flex align-items-center flex-wrap mr-1">
            <!--begin::Page Heading-->
            <div class="d-flex align-items-baseline flex-wrap mr-5">
              <!--begin::Page Title-->
              <h5 class="text-dark font-weight-bold my-1 mr-5">Modifier l'Agence</h5>
              <!--end::Page Title-->
              <!--begin::Breadcrumb-->
              <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                <li class="breadcrumb-item">
                  <a href="{{ route('list-agences') }}" class="text-muted">Agences</a>
                </li>
                <li class="breadcrumb-item">
                  <a href="{{ route('show-agence', ['agence' => $agence->id]) }}" class="text-muted">{{ $agence->libelle }}</a>
                </li>
                <li class="breadcrumb-item">
                  <a href="{{ route('edit-agence', ['agence' => $agence->id]) }}" class="text-muted">Modifier l'Agence</a>
                </li>
              </ul>
              <!--end::Breadcrumb-->
            </div>
            <!--end::Page Heading-->
          </div>
          <!--end::Info-->
          <!--begin::Toolbar-->
          <div class="d-flex align-items-center">
            <a href="{{ url()->previous() }}" class="btn btn-light-primary font-weight-bolder">
              <span class="svg-icon svg-icon-md mr-1">
                <!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Left-2.svg-->
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                  <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <polygon points="0 0 24 0 24 24 0 24"></polygon>
                    <rect fill="#000000" opacity="0.3" transform="translate(15.000000, 12.000000) scale(-1, 1) rotate(-90.000000) translate(-15.000000, -12.000000)" x="14" y="7" width="2" height="10" rx="1"></rect>
                    <path d="M3.7071045,15.7071045 C3.3165802,16.0976288 2.68341522,16.0976288 2.29289093,15.7071045 C1.90236664,15.3165802 1.90236664,14.6834152 2.29289093,14.2928909 L8.29289093,8.29289093 C8.67146987,7.914312 9.28105631,7.90106637 9.67572234,8.26284357 L15.6757223,13.7628436 C16.0828413,14.136036 16.1103443,14.7686034 15.7371519,15.1757223 C15.3639594,15.5828413 14.7313921,15.6103443 14.3242731,15.2371519 L9.03007346,10.3841355 L3.7071045,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(9.000001, 11.999997) scale(-1, -1) rotate(90.000000) translate(-9.000001, -11.999997)"></path>
                  </g>
                </svg>
                <!--end::Svg Icon-->
              </span>
              Retour
            </a>
          </div>
          <!--end::Toolbar-->
        </div>
      </div>
      <!--end::Subheader-->
      <div class="content flex-column-fluid" id="kt_content">
        <!--begin::Card-->
        <form method="post" action="{{ route('edit-agence', ['agence' => $agence->id]) }}" class="form-xhr form fv-plugins-bootstrap fv-plugins-framework" id="kt_form">
          @csrf
          <input type="hidden" name="continue" value="{{ url()->previous() }}">
          <div class="card card-custom card-shadowless rounded-top-0">
            <!--begin::Body-->
            <div class="card-body">
              <div class="row justify-content-center py-4 px-8 py-lg-8 px-lg-10">
                <div class="col-xl-12 col-xxl-10">
                  <div class="row justify-content-center">
                    <div class="col-xl-9">
                      <div class="my-5">
                        <h5 class="text-dark font-weight-bold mb-10">Information sur l'Agence :</h5>
                      <!--begin::Group-->
                      <div class="form-group row fv-plugins-icon-container">
                        <label class="col-xl-3 col-lg-3 col-form-label">Libellé</label>
                        <div class="col-lg-9 col-xl-9">
                          <input class="form-control form-control-solid form-control-lg" name="libelle" type="text" value="{{ $agence->libelle }}">
                          <div class="fv-plugins-message-container"></div>
                        </div>
                      </div>
                      <!--end::Group-->
                      <!--begin::Group-->
                      <div class="form-group row fv-plugins-icon-container">
                        <label class="col-xl-3 col-lg-3 col-form-label">Localisation</label>
                        <div class="col-lg-9 col-xl-9">
                          <input class="form-control form-control-solid form-control-lg" name="localisation" type="text" value="{{ $agence->localisation }}">
                          <div class="fv-plugins-message-container"></div>
                        </div>
                      </div>
                      <!--end::Group-->
                      <!--begin::Group-->
                      <div class="form-group row fv-plugins-icon-container">
                        <label class="col-xl-3 col-lg-3 col-form-label">Contact</label>
                        <div class="col-lg-9 col-xl-9">
                          <input class="form-control form-control-solid form-control-lg" name="contact" type="text" value="{{ $agence->contact }}">
                          <div class="fv-plugins-message-container"></div>
                        </div>
                      </div>
                      <!--end::Group-->
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!--end::Body-->
          <!--begin::Actions-->
          <div class="card-footer">
            <div class="row justify-content-center px-8 px-lg-10">
              <div class="col-xl-12 col-xxl-10">
                <div class="row justify-content-center">
                  <div class="col-xl-6">
                    <button type="submit" class="submitter btn-lg btn btn-primary font-weight-bold mr-2" id="kt_btn_spinner">Enregistrer</button>
                    <button type="reset" class="btn-lg btn btn-clean font-weight-bold">Annuler</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!--end::Actions-->
        </div>
      </form>
    </div>
  </div>
</div>
@endsection
