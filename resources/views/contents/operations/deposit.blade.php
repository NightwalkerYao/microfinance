@extends('layouts.dashboard')

@section('title')
  Faire un Dépôt
@endsection

@section('content')
  <div class="d-flex flex-row flex-column-fluid container">
    <!--begin::Content Wrapper-->
    <div class="main d-flex flex-column flex-row-fluid">
      <!--begin::Subheader-->
      <div class="subheader py-2 py-lg-6" id="kt_subheader">
        <div class="w-100 d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
          <!--begin::Info-->
          <div class="d-flex align-items-center flex-wrap mr-1">
            <!--begin::Page Heading-->
            <div class="d-flex align-items-baseline flex-wrap mr-5">
              <!--begin::Page Title-->
              <h5 class="text-dark font-weight-bold my-1 mr-5">Déposer de l'argent</h5>
              <!--end::Page Title-->
              <!--begin::Breadcrumb-->
              <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                <li class="breadcrumb-item">
                  <a href="{{ route('list-operations') }}" class="text-muted">Opérations</a>
                </li>
                <li class="breadcrumb-item">
                  <a href="{{ route('make-depot') }}" class="text-muted">Dépôt</a>
                </li>
              </ul>
              <!--end::Breadcrumb-->
            </div>
            <!--end::Page Heading-->
          </div>
          <!--end::Info-->
          <!--begin::Toolbar-->
          <div class="d-flex align-items-center">
            <a href="{{ url()->previous() }}" class="btn btn-light-primary font-weight-bolder">
              <span class="svg-icon svg-icon-md mr-1">
                <!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Left-2.svg-->
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                  <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <polygon points="0 0 24 0 24 24 0 24"></polygon>
                    <rect fill="#000000" opacity="0.3" transform="translate(15.000000, 12.000000) scale(-1, 1) rotate(-90.000000) translate(-15.000000, -12.000000)" x="14" y="7" width="2" height="10" rx="1"></rect>
                    <path d="M3.7071045,15.7071045 C3.3165802,16.0976288 2.68341522,16.0976288 2.29289093,15.7071045 C1.90236664,15.3165802 1.90236664,14.6834152 2.29289093,14.2928909 L8.29289093,8.29289093 C8.67146987,7.914312 9.28105631,7.90106637 9.67572234,8.26284357 L15.6757223,13.7628436 C16.0828413,14.136036 16.1103443,14.7686034 15.7371519,15.1757223 C15.3639594,15.5828413 14.7313921,15.6103443 14.3242731,15.2371519 L9.03007346,10.3841355 L3.7071045,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(9.000001, 11.999997) scale(-1, -1) rotate(90.000000) translate(-9.000001, -11.999997)"></path>
                  </g>
                </svg>
                <!--end::Svg Icon-->
              </span>
              Retour
            </a>
          </div>
          <!--end::Toolbar-->
        </div>
      </div>
      <!--end::Subheader-->
      <div class="content flex-column-fluid" id="kt_content">
        <!--begin::Form-->
        <form class="form form-xhr" method="post" action="{{ route('make-depot') }}" >
          <!--begin::Card-->
          @csrf

          <div class="card card-custom gutter-b example example-compact">
            <div class="card-header">
              <h2 class="card-title">DEPOT D'ARGENT</h2>
              <div class="card-toolbar">
                <div class="example-tools justify-content-center">
                  <h3>MICRO FINANCE IVOIRE</h3>
                </div>
              </div>
            </div>
            <div class="card-body px-10">
              <div class="row">
                <div class="col-md-6 px-10">
                  <h5 class="text-dark font-weight-bold mb-10">Détails du Compte :</h5>
                  <!--begin::Group-->
                  <div class="form-group row">
                    <div class="image-input image-input-outline" id="kt_user_add_avatar"></div>
                  </div>
                  <!--end::Group-->
                  <!--begin::Group-->
                  <div class="form-group">
                    <label>Compte :</label>
                    <select class="form-control border-2 border-primary form-control-lg form-control-select custom-select select2" id="kt_select2_6"  name="compte_id" id="select-compte">
                      <option value="">Entrez le Numéro du Compte</option>
                    </select>
                  </div>
                  <!--end::Group-->
                  <!--begin::Group-->
                  <div class="form-group">
                    <label>Nom du Client :</label>
                    <input class="form-control border-2 border-primary form-control-lg" type="text" readonly id="client_name">
                  </div>
                  <!--end::Group-->
                </div>
                <div class="col-md-6 px-10">
                  <h5 class="text-dark font-weight-bold mb-10">Détails de l'Opération :</h5>
                  <!--begin::Group-->
                  <div class="form-group">
                    <label>Numéro de l'Opération :</label>
                    <input class="form-control border-2 border-primary form-control-lg" name="numero" type="text" value="{{ "011".str_pad(App\Models\Operation::count()+1, 7, mt_rand(1000, 10000), STR_PAD_LEFT) }}">
                  </div>
                  <!--end::Group-->
                  <!--begin::Group-->
                  <div class="form-group">
                    <label>Montant à déposer :</label>
                    <input class="form-control border-2 border-primary form-control-lg" name="montant" type="number" min="0" value="0">
                  </div>
                  <!--end::Group-->
                  <!--begin::Group-->
                  <div class="form-group">
                    <label>Mode d'Opération :</label>
                    <select class="form-control border-2 border-primary form-control-select custom-select select-2"  name="mode_operation_id">
                      @foreach ($modes_operation->sortBy('libelle') as $mo)
                        <option value="{{ $mo->id }}">{{ $mo->libelle }}</option>
                      @endforeach
                    </select>
                  </div>
                  <!--end::Group-->
                  <!--begin::Group-->
                  <div class="form-group">
                    <label>Agence :</label>
                    <select class="form-control border-2 border-primary form-control-select custom-select select-2"  name="agence_id">
                      @foreach ($agences->sortBy('libelle') as $agence)
                        <option value="{{ $agence->id }}">{{ $agence->libelle }}</option>
                      @endforeach
                    </select>
                  </div>
                  <!--end::Group-->
                  <!--begin::Group-->
                  <div class="form-group">
                    <label>Observation :</label>
                    <textarea name="observation" rows="4" class="form-control border-2 border-primary form-control-lg form-control-textarea"></textarea>
                  </div>
                  <!--end::Group-->
                </div>
              </div>
            </div>
            <!--begin::Actions-->
            <div class="card-footer">
              <div class="row justify-content-center px-8 px-lg-10">
                <div class="col-xl-12 col-xxl-10">
                  <div class="row justify-content-center">
                    <div class="col-xl-6">
                      <button type="submit" class="submitter btn-lg btn btn-primary font-weight-bold mr-2" id="kt_btn_spinner">Valider le Dépôt</button>
                      <button type="reset" class="btn-lg btn btn-clean font-weight-bold">Annuler</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!--end::Actions-->
          </div>
          <!--end::Card-->
        </form>
        <!--end::Form-->
      </div>
      <!--end::Content-->
    </div>
    <!--end::Content Wrapper-->
  </div>
@endsection

@section('specific-js')
  <script type="text/javascript">
  $('#kt_user_add_avatar').hide();

  // Class definition
  var KTSelect2 = function() {
    // Private functions
    var demos = function() {
      // loading remote data
      function formatCompte(c) {
        if (!c.numero) return c.text;
        return c.numero;
      }

      function formatCompteSelection(c) {
        if(!c.numero) return c.text;
        $('#kt_user_add_avatar').hide();
        $('#client_name').val(c.client.prenom+' '+c.client.nom.toUpperCase());
        if(c.client.photo) {
          $('#kt_user_add_avatar').html(`<div class="image-input-wrapper" style="background-image: url(/storage/`+c.client.photo+`)"></div>`).show();
        } else {
          $('#kt_user_add_avatar').html(`<div class="image-input-wrapper" style="background-image: url(/assets/media/users/blank.jpg)"></div>`).show();
        }
        return c.numero;
      }

      $("#kt_select2_6").select2({
        placeholder: "Chercher un Compte",
        allowClear: true,
        ajax: {
          url: "{{ route('json-comptes') }}",
          dataType: 'json',
          delay: 250,
          data: function(params) {
            return {
              q: params.term, // search term
              page: params.page || 1
            };
          },
          processResults: function(data, params) {
            params.page = params.page || 1;
            return {
              results: data.data,
              pagination: {
                more: (params.page * 30) < data.total_count
              }
            };
          },
          cache: true
        },
        escapeMarkup: function(markup) {
          return markup;
        }, // let our custom formatter work
        minimumInputLength: 1,
        templateResult: formatCompte, // omitted for brevity, see the source of this page
        templateSelection: formatCompteSelection // omitted for brevity, see the source of this page
      });

    }
    // Public functions
    return {
      init: function() {
        demos();
      }
    };
  }();

  // Initialization
  jQuery(document).ready(function() {
    KTSelect2.init();
  });


  </script>
@endsection
