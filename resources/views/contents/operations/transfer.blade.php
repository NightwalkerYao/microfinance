@extends('layouts.dashboard')

@section('title')
  Faire un Transfert
@endsection

@section('content')
  <div class="d-flex flex-row flex-column-fluid container">
    <!--begin::Content Wrapper-->
    <div class="main d-flex flex-column flex-row-fluid">
      <!--begin::Subheader-->
      <div class="subheader py-2 py-lg-6" id="kt_subheader">
        <div class="w-100 d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
          <!--begin::Info-->
          <div class="d-flex align-items-center flex-wrap mr-1">
            <!--begin::Page Heading-->
            <div class="d-flex align-items-baseline flex-wrap mr-5">
              <!--begin::Page Title-->
              <h5 class="text-dark font-weight-bold my-1 mr-5">Transférer de l'argent</h5>
              <!--end::Page Title-->
              <!--begin::Breadcrumb-->
              <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                <li class="breadcrumb-item">
                  <a href="{{ route('list-operations') }}" class="text-muted">Opérations</a>
                </li>
                <li class="breadcrumb-item">
                  <a href="{{ route('make-transfert') }}" class="text-muted">Transfert</a>
                </li>
              </ul>
              <!--end::Breadcrumb-->
            </div>
            <!--end::Page Heading-->
          </div>
          <!--end::Info-->
          <!--begin::Toolbar-->
          <div class="d-flex align-items-center">
            <a href="{{ url()->previous() }}" class="btn btn-light-primary font-weight-bolder">
              <span class="svg-icon svg-icon-md mr-1">
                <!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Left-2.svg-->
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                  <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <polygon points="0 0 24 0 24 24 0 24"></polygon>
                    <rect fill="#000000" opacity="0.3" transform="translate(15.000000, 12.000000) scale(-1, 1) rotate(-90.000000) translate(-15.000000, -12.000000)" x="14" y="7" width="2" height="10" rx="1"></rect>
                    <path d="M3.7071045,15.7071045 C3.3165802,16.0976288 2.68341522,16.0976288 2.29289093,15.7071045 C1.90236664,15.3165802 1.90236664,14.6834152 2.29289093,14.2928909 L8.29289093,8.29289093 C8.67146987,7.914312 9.28105631,7.90106637 9.67572234,8.26284357 L15.6757223,13.7628436 C16.0828413,14.136036 16.1103443,14.7686034 15.7371519,15.1757223 C15.3639594,15.5828413 14.7313921,15.6103443 14.3242731,15.2371519 L9.03007346,10.3841355 L3.7071045,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(9.000001, 11.999997) scale(-1, -1) rotate(90.000000) translate(-9.000001, -11.999997)"></path>
                  </g>
                </svg>
                <!--end::Svg Icon-->
              </span>
              Retour
            </a>
          </div>
          <!--end::Toolbar-->
        </div>
      </div>
      <!--end::Subheader-->
      <div class="content flex-column-fluid" id="kt_content">
        <!--begin::Form-->
        <form class="form form-xhr" method="post" action="{{ route('make-transfert') }}" >
          <!--begin::Card-->
          @csrf

          <div class="row">

            <div class="col-lg-4">
              <div class="card card-custom card-stretch">
                <div class="card-header">
                  <div class="card-title">
                    <span class="card-icon">
                      <i class="flaticon2-paperplane text-primary"></i>
                    </span>
                    <h3 class="card-label">
                      Origine
                      <small>Sélectionnez un compte</small>
                    </h3>
                  </div>
                </div>
                <div class="card-body d-flex flex-column justify-content-between">
                  <div>
                    <!--begin::Group-->
                    <div class="form-group">
                      <div class="">
                        <!--begin::Stats Widget 29-->
                        <div class="card card-custom bgi-no-repeat card-stretch gutter-b" style="background-position: right top; background-size: 30% auto; background-image: url(/assets/media/svg/shapes/abstract-1.svg)" id="solde-card">
                          <!--begin::Body-->
                          <div class="card-body">
                            <span class="svg-icon svg-icon-2x svg-icon-info">
                              <!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Mail-opened.svg-->
                              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                  <rect x="0" y="0" width="24" height="24"></rect>
                                  <path d="M6,2 L18,2 C18.5522847,2 19,2.44771525 19,3 L19,12 C19,12.5522847 18.5522847,13 18,13 L6,13 C5.44771525,13 5,12.5522847 5,12 L5,3 C5,2.44771525 5.44771525,2 6,2 Z M7.5,5 C7.22385763,5 7,5.22385763 7,5.5 C7,5.77614237 7.22385763,6 7.5,6 L13.5,6 C13.7761424,6 14,5.77614237 14,5.5 C14,5.22385763 13.7761424,5 13.5,5 L7.5,5 Z M7.5,7 C7.22385763,7 7,7.22385763 7,7.5 C7,7.77614237 7.22385763,8 7.5,8 L10.5,8 C10.7761424,8 11,7.77614237 11,7.5 C11,7.22385763 10.7761424,7 10.5,7 L7.5,7 Z" fill="#000000" opacity="0.3"></path>
                                  <path d="M3.79274528,6.57253826 L12,12.5 L20.2072547,6.57253826 C20.4311176,6.4108595 20.7436609,6.46126971 20.9053396,6.68513259 C20.9668779,6.77033951 21,6.87277228 21,6.97787787 L21,17 C21,18.1045695 20.1045695,19 19,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,6.97787787 C3,6.70173549 3.22385763,6.47787787 3.5,6.47787787 C3.60510559,6.47787787 3.70753836,6.51099993 3.79274528,6.57253826 Z" fill="#000000"></path>
                                </g>
                              </svg>
                              <!--end::Svg Icon-->
                            </span>
                            <span class="card-title font-weight-bolder text-dark-75 font-size-h2 mb-0 mt-6 d-block" id="show-solde"></span>
                            <span class="font-weight-bold text-muted font-size-sm">Solde actuel</span>
                          </div>
                          <!--end::Body-->
                        </div>
                        <!--end::Stats Widget 29-->
                      </div>
                    </div>
                    <!--end::Group-->
                    <!--begin::Group-->
                    <div class="form-group">
                      <label>Compte :</label>
                      <select class="form-control form-control-solid form-control-lg form-control-select custom-select select2" id="kt_select2_6"  name="compte_id" id="select-compte">
                        <option value="">Entrez le Numéro du Compte</option>
                      </select>
                    </div>
                    <!--end::Group-->
                    <!--begin::Group-->
                    <div class="form-group">
                      <label>Nom du Client :</label>
                      <input class="form-control form-control-solid form-control-lg" type="text" readonly id="client_name">
                    </div>
                    <!--end::Group-->
                  </div>
                </div>
              </div>
            </div>


            <div class="col-lg-4">
              <div class="card card-custom card-stretch">
                <div class="card-header">
                  <div class="card-title">
                    <span class="card-icon">
                      <i class="flaticon2-download-1 text-primary"></i>
                    </span>
                    <h3 class="card-label">
                      Destination
                      <small>Sélectionnez un compte</small>
                    </h3>
                  </div>
                </div>
                <div class="card-body d-flex flex-column justify-content-between">
                  <div>
                    <!--begin::Group-->
                    <div class="form-group">
                      <div class="">
                        <!--begin::Stats Widget 29-->
                        <div class="card card-custom bgi-no-repeat bg-light-warning card-stretch gutter-b" style="background-position: right top; background-size: 30% auto; background-image: url(/assets/media/svg/shapes/abstract-4.svg)" id="solde-card-d">
                          <!--begin::Body-->
                          <div class="card-body">
                            <span class="svg-icon svg-icon-2x svg-icon-info">
                              <!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Mail-opened.svg-->
                              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                  <rect x="0" y="0" width="24" height="24"></rect>
                                  <path d="M6,2 L18,2 C18.5522847,2 19,2.44771525 19,3 L19,12 C19,12.5522847 18.5522847,13 18,13 L6,13 C5.44771525,13 5,12.5522847 5,12 L5,3 C5,2.44771525 5.44771525,2 6,2 Z M7.5,5 C7.22385763,5 7,5.22385763 7,5.5 C7,5.77614237 7.22385763,6 7.5,6 L13.5,6 C13.7761424,6 14,5.77614237 14,5.5 C14,5.22385763 13.7761424,5 13.5,5 L7.5,5 Z M7.5,7 C7.22385763,7 7,7.22385763 7,7.5 C7,7.77614237 7.22385763,8 7.5,8 L10.5,8 C10.7761424,8 11,7.77614237 11,7.5 C11,7.22385763 10.7761424,7 10.5,7 L7.5,7 Z" fill="#000000" opacity="0.3"></path>
                                  <path d="M3.79274528,6.57253826 L12,12.5 L20.2072547,6.57253826 C20.4311176,6.4108595 20.7436609,6.46126971 20.9053396,6.68513259 C20.9668779,6.77033951 21,6.87277228 21,6.97787787 L21,17 C21,18.1045695 20.1045695,19 19,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,6.97787787 C3,6.70173549 3.22385763,6.47787787 3.5,6.47787787 C3.60510559,6.47787787 3.70753836,6.51099993 3.79274528,6.57253826 Z" fill="#000000"></path>
                                </g>
                              </svg>
                              <!--end::Svg Icon-->
                            </span>
                            <span class="card-title font-weight-bolder text-dark-75 font-size-h2 mb-0 mt-6 d-block" id="show-solde-d"></span>
                            <span class="font-weight-bold text-muted font-size-sm">Solde actuel</span>
                          </div>
                          <!--end::Body-->
                        </div>
                        <!--end::Stats Widget 29-->
                      </div>
                    </div>
                    <!--end::Group-->
                    <!--begin::Group-->
                    <div class="form-group">
                      <label>Compte :</label>
                      <select class="form-control form-control-solid form-control-lg form-control-select custom-select select2" id="kt_select2_6_1"  name="compte_dest_id" id="select-compte-d">
                        <option value="">Entrez le Numéro du Compte</option>
                      </select>
                    </div>
                    <!--end::Group-->
                    <!--begin::Group-->
                    <div class="form-group">
                      <label>Nom du Client :</label>
                      <input class="form-control form-control-solid form-control-lg" type="text" readonly id="client_name-d">
                    </div>
                    <!--end::Group-->
                  </div>
                </div>
              </div>
            </div>


            <div class="col-lg-4">
              <div class="card card-custom card-stretch">
                <div class="card-header">
                  <div class="card-title">
                    <span class="card-icon">
                      <i class="flaticon2-information text-primary"></i>
                    </span>
                    <h3 class="card-label">
                      Détails
                      <small>A propos de l'opération</small>
                    </h3>
                  </div>
                </div>
                <div class="card-body d-flex flex-column justify-content-between">
                  <!--begin::Group-->
                  <div class="form-group">
                    <label>Numéro de l'Opération :</label>
                    <input class="form-control form-control-solid form-control-lg" name="numero" type="text" value="{{ "013".str_pad(App\Models\Operation::count()+1, 7, mt_rand(1000, 10000), STR_PAD_LEFT) }}">
                  </div>
                  <!--end::Group-->
                  <!--begin::Group-->
                  <div class="form-group">
                    <label>Montant à transférer :</label>
                    <input class="form-control form-control-solid form-control-lg" name="montant" type="number" min="0" value="1">
                  </div>
                  <!--end::Group-->
                  <!--begin::Group-->
                  <div class="form-group">
                    <label>Agence :</label>
                    <select class="form-control form-control-solid form-control-select custom-select select-2"  name="agence_id">
                      @foreach ($agences->sortBy('libelle') as $agence)
                        <option value="{{ $agence->id }}">{{ $agence->libelle }}</option>
                      @endforeach
                    </select>
                  </div>
                  <!--end::Group-->
                  <!--begin::Group-->
                  <div class="form-group">
                    <label>Observation :</label>
                    <textarea name="observation" rows="4" class="form-control form-control-solid form-control-lg form-control-textarea"></textarea>
                  </div>
                  <!--end::Group-->
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="row mt-10 justify-content-center">
                <div class="col-xl-12 text-right text-align-right">
                  <button type="reset" class="btn-lg btn btn-clean font-weight-bold">Annuler</button>
                  <button type="submit" class="submitter btn-lg btn btn-primary font-weight-bold mr-2" id="kt_btn_spinner">Valider le Transfert</button>
                </div>
              </div>
            </div>

          </div>

        </form>
        <!--end::Form-->
      </div>
      <!--end::Content-->
    </div>
    <!--end::Content Wrapper-->
  </div>
@endsection

@section('specific-js')
  <script type="text/javascript">
  $('#solde-card,#solde-card-d').hide();

  // Class definition
  var KTSelect2 = function() {
    // Private functions
    var demos = function() {
      // loading remote data
      function formatCompte(c) {
        if (!c.numero) return c.text;
        return c.numero;
      }

      function formatCompteSelection(c) {
        if(!c.numero) return c.text;
        $('#solde-card').hide();
        $('#client_name').val(c.client.prenom+' '+c.client.nom.toUpperCase());
        $('#show-solde').text(c.solde+ ' FCFA');
        $('#solde-card').show();
        return c.numero;
      }

      function formatCompte2(c) {
        if (!c.numero) return c.text;
        return c.numero;
      }

      function formatCompteSelection2(c) {
        if(!c.numero) return c.text;
        $('#solde-card-d').hide();
        $('#client_name-d').val(c.client.prenom+' '+c.client.nom.toUpperCase());
        $('#show-solde-d').text(c.solde+ ' FCFA');
        $('#solde-card-d').show();
        return c.numero;
      }

      $("#kt_select2_6").select2({
        placeholder: "Chercher un Compte",
        allowClear: true,
        ajax: {
          url: "{{ route('json-comptes') }}",
          dataType: 'json',
          delay: 250,
          data: function(params) {
            return {
              q: params.term, // search term
              page: params.page || 1
            };
          },
          processResults: function(data, params) {
            params.page = params.page || 1;
            return {
              results: data.data,
              pagination: {
                more: (params.page * 30) < data.total_count
              }
            };
          },
          cache: true
        },
        escapeMarkup: function(markup) {
          return markup;
        }, // let our custom formatter work
        minimumInputLength: 1,
        templateResult: formatCompte, // omitted for brevity, see the source of this page
        templateSelection: formatCompteSelection // omitted for brevity, see the source of this page
      });

      $("#kt_select2_6_1").select2({
        placeholder: "Chercher un Compte",
        allowClear: true,
        ajax: {
          url: "{{ route('json-comptes') }}",
          dataType: 'json',
          delay: 250,
          data: function(params) {
            return {
              q: params.term, // search term
              page: params.page || 1
            };
          },
          processResults: function(data, params) {
            params.page = params.page || 1;
            return {
              results: data.data,
              pagination: {
                more: (params.page * 30) < data.total_count
              }
            };
          },
          cache: true
        },
        escapeMarkup: function(markup) {
          return markup;
        }, // let our custom formatter work
        minimumInputLength: 1,
        templateResult: formatCompte2, // omitted for brevity, see the source of this page
        templateSelection: formatCompteSelection2 // omitted for brevity, see the source of this page
      });

    }
    // Public functions
    return {
      init: function() {
        demos();
      }
    };
  }();

  // Initialization
  jQuery(document).ready(function() {
    KTSelect2.init();
  });


  </script>
@endsection
