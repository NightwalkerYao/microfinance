@extends('layouts.dashboard')

@section('title')
  Opérations
@endsection

@section('content')
  <div class="d-flex flex-row flex-column-fluid container">
    <!--begin::Content Wrapper-->
    <div class="main d-flex flex-column flex-row-fluid">
      <!--begin::Subheader-->
      <div class="subheader py-2 py-lg-6" id="kt_subheader">
        <div class="w-100 d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
          <!--begin::Info-->
          <div class="d-flex align-items-center flex-wrap mr-1">
            <!--begin::Page Heading-->
            <div class="d-flex align-items-baseline flex-wrap mr-5">
              <!--begin::Page Title-->
              <h5 class="text-dark font-weight-bold my-1 mr-5">Liste des Opérations</h5>
              <!--end::Page Title-->
              <!--begin::Breadcrumb-->
              <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                <li class="breadcrumb-item">
                  <a href="{{ route('list-operations-client') }}" class="text-muted">Opérations</a>
                </li>
              </ul>
              <!--end::Breadcrumb-->
            </div>
            <!--end::Page Heading-->
          </div>
          <!--end::Info-->
          <!--begin::Toolbar-->
          <div class="d-flex align-items-center">
            <a href="{{ url()->previous() }}" class="btn btn-light-primary font-weight-bolder">
              <span class="svg-icon svg-icon-md mr-1">
                <!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Left-2.svg-->
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                  <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <polygon points="0 0 24 0 24 24 0 24"></polygon>
                    <rect fill="#000000" opacity="0.3" transform="translate(15.000000, 12.000000) scale(-1, 1) rotate(-90.000000) translate(-15.000000, -12.000000)" x="14" y="7" width="2" height="10" rx="1"></rect>
                    <path d="M3.7071045,15.7071045 C3.3165802,16.0976288 2.68341522,16.0976288 2.29289093,15.7071045 C1.90236664,15.3165802 1.90236664,14.6834152 2.29289093,14.2928909 L8.29289093,8.29289093 C8.67146987,7.914312 9.28105631,7.90106637 9.67572234,8.26284357 L15.6757223,13.7628436 C16.0828413,14.136036 16.1103443,14.7686034 15.7371519,15.1757223 C15.3639594,15.5828413 14.7313921,15.6103443 14.3242731,15.2371519 L9.03007346,10.3841355 L3.7071045,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(9.000001, 11.999997) scale(-1, -1) rotate(90.000000) translate(-9.000001, -11.999997)"></path>
                  </g>
                </svg>
                <!--end::Svg Icon-->
              </span>
              Retour
            </a>
          </div>
          <!--end::Toolbar-->
        </div>
      </div>
      <!--end::Subheader-->
      <div class="content flex-column-fluid" id="kt_content">
        <div class="card card-custom gutter-b">
          <!--begin::Header-->
          <div class="card-header border-0 py-5">
            <h3 class="card-title align-items-start flex-column">
              <span class="card-label font-weight-bolder text-dark">Opérations</span>
              <span class="text-muted mt-3 font-weight-bold font-size-sm">Liste des {{ $count }} Opérations</span>
            </h3>
          </div>
          <!--end::Header-->
          <!--begin::Body-->
          <div class="card-body">

            <div class="mb-7">
              <div class="row align-items-center">
                <div class="col-lg-10 col-xl-10">
                  <div class="row align-items-center">
                    <div class="col-md-4 my-2 my-md-0">
                      <div class="input-icon">
                        <input type="text" class="form-control" placeholder="Numéro d'opération..." id="kt_datatable_search_query" />
                        <span>
                          <i class="flaticon2-search-1 text-muted"></i>
                        </span>
                      </div>
                    </div>
                    <div class="col-md-4 my-2 my-md-0">
                      <div class="d-flex align-items-center">
                        <label class="mr-3 mb-0 d-none d-md-block">Type: </label>
                        <select class="form-control custom-select" id="kt_datatable_search_type">
                          <option value="all">Tout</option>
                          @foreach ($types_operation as $t)
                            <option value="{{ $t->id }}">{{ $t->libelle }}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="col-md-4 my-2 my-md-0">
                      <div class="d-flex align-items-center">
                        <label class="mr-3 mb-0 d-none d-md-block">Mode: </label>
                        <select class="form-control custom-select" id="kt_datatable_search_mode">
                          <option value="all">Tout</option>
                          @foreach ($modes_operation as $m)
                            <option value="{{ $m->id }}">{{ $m->libelle }}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-2 col-xl-2 mt-5 mt-lg-0">
                  <a class="btn btn-light-primary px-6 font-weight-bold" id="kt_search_trigger">Chercher</a>
                </div>
              </div>
            </div>

            <!--begin: Datatable-->
            <div class="datatable datatable-bordered datatable-head-custom" id="kt_datatable"></div>
            <!--end: Datatable-->

          </div>
          <!--end::Body-->
        </div>
      </div>
      <!--end::Content-->
    </div>
    <!--end::Content Wrapper-->
  </div>
@endsection

@section('specific-js')
  <script type="text/javascript">
  "use strict";
  // Class definition

  var KTDatatableRemoteAjax = function() {
    // Private functions

    // basic demo
    var demo = function() {

      var datatable = $('#kt_datatable').KTDatatable({
        // datasource definition
        data: {
          type: 'remote',
          source: {
            read: {
              method: 'get',
              url: window.location.href,
              // sample custom headers
              // headers: {'x-my-custom-header': 'some value', 'x-test-header': 'the value'},
              map: function(raw) {
                // sample data mapping
                var dataSet = raw;
                if (typeof raw.data !== 'undefined') {
                  dataSet = raw.data;
                }
                return dataSet;
              },
            },
          },
          pageSize: 10,
          serverPaging: true,
          serverFiltering: true,
          serverSorting: true,
          saveState: false
        },

        // layout definition
        layout: {
          scroll: false,
          footer: false,
          spinner: {
            message: "Chargement des données..."
          }
        },

        // column sorting
        sortable: true,

        pagination: true,

        search: {
          input: $('#kt_datatable_search_query'),
          key: 'q',
          delay: 400
        },

        translate: {
          records: {
            processing: "Chargement des données...",
            noRecords: "Il n'y a rien à afficher."
          },
          toolbar: {
            pagination: {
              items: {
                default: {
                  first: "Début",
                  prec: "Précédent",
                  next: "Suivant",
                  last: "Fin",
                  more: "Plus",
                  input: "Numéro de page",
                  select: "Sélectionner la page"
                },
                info: "{{ str_replace(["(", ")"], ["{", "}"], 'Opérations ((start)) - ((end)) sur ((total))') }}"
              }
            }
          }
        },

        // columns definition
        columns: [{
          field: 'nom_complet',
          title: 'Client',
          width: 300,
          template: function(row) {
            let output = `
            <div class="d-flex align-items-center">
            <div class="symbol symbol-50 flex-shrink-0 mr-4">
            <div class="symbol-label" style="background-image: url('`+row.photo+`')"></div>
            </div>
            <div>
            <a href="{{ route("show-client", ['client' => "__id__"]) }}" class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">`+row.nom_complet+`</a>
            <span class="text-muted font-weight-bold d-block">`+ row.compte.numero +`</span>
            </div>
            </div>
            `;
            return output.replace(/__id__/g, row.compte.client_id);
          }
        },{
          field: 'numero',
          title: 'Numéro',
          width: 120,
          template: function(row) {
            return `<span class="text-dark-50 font-weight-bolder d-block font-size-lg">`+row.numero+`</span>`;
          }
        }, {
          field: 'type_operation',
          title: 'Type d\'Op.',
          width: 120,
          template: function(row) {
            return `<span class="text-muted font-weight-bolder d-block font-size-lg">`+row.type.libelle+`</span>`;
          }
        }, {
          field: 'montant',
          title: 'Montant',
          width: 120,
          autoHide: false,
          template: function(row) {
            return `<span class="text-muted font-weight-bolder d-block font-size-lg">`+row.montant+` F</span>`
          },
        },{
          field: 'agence',
          title: 'Agence',
          width: 120,
          autoHide: false,
          template: function(row) {
            return `<span class="text-muted font-weight-bolder d-block font-size-lg">`+row.agence.libelle+`</span>`
          },
        }, {
          field: 'created_at',
          title: 'Date',
          width: 130,
          type: 'date',
          format: 'DD/MM/YYYY',
          template: function(row) {
            return `<span class="text-muted font-weight-bolder d-block font-size-lg">`+row.created_at+`</span>`
          }
        }],

      });

      // $('#kt_datatable_search_agence').on('change', function() {
      //   datatable.search($(this).val().toLowerCase(), 'agence');
      // });
      $('#kt_datatable_search_type').on('change', function() {
        datatable.search($(this).val().toLowerCase(), 'type');
      });
      $('#kt_datatable_search_mode').on('change', function() {
        datatable.search($(this).val().toLowerCase(), 'mode');
      });

      $('#kt_datatable_search_agence').selectpicker();
      $('#kt_datatable_search_type').selectpicker();
      $('#kt_datatable_search_mode').selectpicker();

      $('#kt_search_trigger').click(function(e){
        e.preventDefault();
        datatable.setDataSourceParam("query[q]", $("#kt_datatable_search_query").val());
        // datatable.setDataSourceParam("query[agence]", $("#kt_datatable_search_agence").val());
        datatable.setDataSourceParam("query[type]", $("#kt_datatable_search_type").val());
        datatable.setDataSourceParam("query[mode]", $("#kt_datatable_search_mode").val());
        datatable.load();
      });
    };

    return {
      // public functions
      init: function() {
        demo();
      },
    };
  }();

  jQuery(document).ready(function() {
    KTDatatableRemoteAjax.init();
  });
  </script>
@endsection
