@extends('layouts.dashboard')

@section('title')
  Compte : {{ $compte->numero }}
@endsection

@section('content')
  <div class="d-flex flex-row flex-column-fluid container">
    <!--begin::Content Wrapper-->
    <div class="main d-flex flex-column flex-row-fluid">
      <!--begin::Subheader-->
      <div class="subheader py-2 py-lg-6" id="kt_subheader">
        <div class="w-100 d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
          <!--begin::Info-->
          <div class="d-flex align-items-center flex-wrap mr-1">
            <!--begin::Page Heading-->
            <div class="d-flex align-items-baseline flex-wrap mr-5">
              <!--begin::Page Title-->
              <h5 class="text-dark font-weight-bold my-1 mr-5">Compte #{{ $compte->numero }}</h5>
              <!--end::Page Title-->
              <!--begin::Breadcrumb-->
              <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                <li class="breadcrumb-item">
                  <a href="{{ route('list-comptes') }}" class="text-muted">Comptes</a>
                </li>
                <li class="breadcrumb-item">
                  <a href="{{ url()->current() }}" class="text-muted">{{ $compte->numero }}</a>
                </li>
              </ul>
              <!--end::Breadcrumb-->
            </div>
            <!--end::Page Heading-->
          </div>
          <!--end::Info-->
        </div>
      </div>
      <!--end::Subheader-->
      <div class="content flex-column-fluid" id="kt_content">
        <!--begin::Card-->
        <div class="card card-custom gutter-b">
          <div class="card-body">
            <!--begin::Details-->
            <div class="d-flex mb-9">
              <!--begin: Pic-->
              <div class="flex-shrink-0 mr-7 mt-lg-0 mt-3">
                <div class="symbol symbol-50 symbol-lg-120">
                  <img src="{{ $compte->client->photo_path }}" alt="image">
                </div>
                <div class="symbol symbol-50 symbol-lg-120 symbol-primary d-none">
                  <span class="font-size-h3 symbol-label font-weight-boldest">MF</span>
                </div>
              </div>
              <!--end::Pic-->
              <!--begin::Info-->
              <div class="flex-grow-1">
                <!--begin::Title-->
                <div class="d-flex justify-content-between flex-wrap mt-1">
                  <div class="d-flex mr-3">
                    <a href="#" class="text-dark-75 text-hover-primary font-size-h5 font-weight-bold mr-3">{{ $compte->client->nom_complet }}</a>
                    @if ($compte->client->user->active)
                      <a href="#">
                        <i class="flaticon2-correct text-success font-size-h5"></i>
                      </a>
                    @endif
                  </div>
                  <div class="my-lg-0 my-3">
                    @if (!$compte->date_fermeture)
                      <a data-href="{{ route('freeze-compte', ['compte' => $compte->id]) }}" onclick="$(this).hiddenPost()" data-confirm="Souhaitez-vous vraiment fermer ce Compte ?" class="confirm btn btn-sm btn-danger font-weight-bolder text-uppercase">FERMER LE COMPTE</a>
                    @else
                      <a data-href="{{ route('freeze-compte', ['compte' => $compte->id]) }}" onclick="$(this).hiddenPost()" class="btn btn-sm btn-success font-weight-bolder text-uppercase">REACTIVER LE COMPTE</a>
                    @endif
                    <a href="{{ route('edit-compte', ['compte' => $compte->id]) }}#observation" class="btn btn-sm btn-dark font-weight-bolder text-uppercase">AJOUTER OBSERVATION</a>
                  </div>
                </div>
                <!--end::Title-->
                <!--begin::Content-->
                <div class="d-flex flex-wrap justify-content-between mt-1">
                  <div class="d-flex flex-column flex-grow-1 pr-8">
                    <div class="d-flex flex-wrap mb-4">
                      <a href="#" class="text-dark-50 text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">
                        <i class="flaticon2-new-email mr-2 font-size-lg"></i>{{ $compte->client->user->email }}
                      </a>
                      <a href="#" class="text-dark-50 text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">
                        <i class="flaticon2-calendar-3 mr-2 font-size-lg"></i>{{ $compte->client->profession }}
                      </a>
                    </div>
                    @if ($compte->client->signature)
                      <span>
                        <a href="/storage/{{ $compte->client->signature }}" target="_blank">
                          <img src="/storage/{{ $compte->client->signature }}" alt="" width="72">
                        </a>
                      </span>
                    @else
                      Pas de Signature.
                    @endif

                  </div>
                </div>
                <!--end::Content-->
              </div>
              <!--end::Info-->
            </div>
            <!--end::Details-->
            <div class="separator separator-solid"></div>
            <!--begin::Items-->
            <div class="d-flex align-items-center flex-wrap mt-8">
              <!--begin::Item-->
              <div class="d-flex align-items-center flex-lg-fill mr-5 mb-2">
                <span class="mr-4">
                  <i class="flaticon-piggy-bank display-4 text-muted font-weight-bold"></i>
                </span>
                <div class="d-flex flex-column text-dark-75">
                  <span class="font-weight-bolder font-size-sm">Dépôts</span>
                  <span class="font-weight-bolder font-size-h5">
                    {{ $compte->calculate_total(1) }} <span class="text-dark-50 font-weight-bold">FCFA</span></span>
                  </div>
                </div>
                <!--end::Item-->
                <!--begin::Item-->
                <div class="d-flex align-items-center flex-lg-fill mr-5 mb-2">
                  <span class="mr-4">
                    <i class="flaticon-confetti display-4 text-muted font-weight-bold"></i>
                  </span>
                  <div class="d-flex flex-column text-dark-75">
                    <span class="font-weight-bolder font-size-sm">Retraits</span>
                    <span class="font-weight-bolder font-size-h5">
                      {{ $compte->calculate_total(2) }} <span class="text-dark-50 font-weight-bold">FCFA</span></span>
                    </div>
                  </div>
                  <!--end::Item-->
                  <!--begin::Item-->
                  <div class="d-flex align-items-center flex-lg-fill mr-5 mb-2">
                    <span class="mr-4">
                      <i class="flaticon-pie-chart display-4 text-muted font-weight-bold"></i>
                    </span>
                    <div class="d-flex flex-column text-dark-75">
                      <span class="font-weight-bolder font-size-sm">Solde</span>
                      <span class="font-weight-bolder font-size-h5">
                        {{ $compte->solde }} <span class="text-dark-50 font-weight-bold">FCFA</span></span>
                      </div>
                    </div>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <div class="d-flex align-items-center flex-lg-fill mr-5 mb-2">
                      <span class="mr-4">
                        <i class="flaticon-file-2 display-4 text-muted font-weight-bold"></i>
                      </span>
                      <div class="d-flex flex-column flex-lg-fill">
                        <span class="text-dark-75 font-weight-bolder font-size-sm">{{ $compte->operations->count() }} opérations</span>
                        <a href="{{ route('list-operations', ['compte' => $compte->id]) }}" class="text-primary font-weight-bolder">Voir</a>
                      </div>
                    </div>
                    <!--end::Item-->
                  </div>
                  <!--begin::Items-->
                </div>
              </div>
              <!--end::Card-->
              <!--begin::Row-->
              <div class="row">
                {{-- <div class="col-lg-1"></div> --}}
                <div class="col-lg-12">
                  <!--begin::Advance Table Widget 2-->
                  <div class="card card-custom card-stretch gutter-b">
                    <!--begin::Header-->
                    <div class="card-header border-0 pt-5">
                      <h3 class="card-title align-items-start flex-column">
                        <span class="card-label font-weight-bolder text-dark">Dernières opérations</span>
                        <span class="text-muted mt-3 font-weight-bold font-size-sm">Les 15 plus recentes</span>
                      </h3>
                    </div>
                    <!--end::Header-->
                    <!--begin::Body-->
                    <div class="card-body pt-2 pb-0 mt-n3">
                      <div class="tab-content mt-5" id="myTabTables11">
                        <!--begin::Tap pane-->
                        <div class="tab-pane- -fade" id="kt_tab_pane_11_1-" role="tabpanel" aria-labelledby="kt_tab_pane_11_1">
                          <!--begin::Table-->
                          <div class="table-responsive">
                            <table class="table table-borderless table-vertical-center">
                              <thead>
                                <tr>
                                  <th class="p-0 w-40px"></th>
                                  <th class="p-0 min-w-200px"></th>
                                  <th class="p-0 min-w-100px"></th>
                                  <th class="p-0 min-w-125px"></th>
                                  <th class="p-0 min-w-110px"></th>
                                  <th class="p-0 min-w-100px"></th>
                                </tr>
                              </thead>
                              <tbody>
                                @foreach ($compte->operations->take(15) as $operation)
                                  <tr>
                                    <td class="pl-0 py-4">
                                      <div class="symbol symbol-50 symbol-light">
                                        <span class="symbol-label">
                                          <img src="/assets/media/svg/misc/003-puzzle.svg" class="h-50 align-self-center" alt="">
                                        </span>
                                      </div>
                                    </td>
                                    <td class="pl-0">
                                      <a href="#" class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">{{ $operation->type->libelle }}</a>
                                      <div>
                                        <span class="font-weight-bolder">Compte : </span>
                                        <a class="text-muted font-weight-bold text-hover-primary" href="#">{{ $compte->numero }}</a>
                                      </div>
                                    </td>
                                    <td class="text-right">
                                      <span class="text-dark-75 font-weight-bolder d-block font-size-lg">{{ $operation->montant }}FCFA</span>
                                      <span class="text-muted font-weight-bold">{{ $operation->mode->libelle }}</span>
                                    </td>
                                    <td class="text-right">
                                      <span class="text-muted font-weight-500">{{ Carbon\Carbon::parse($operation->date)->locale('fr')->diffForHumans() }}</span>
                                    </td>
                                    <td class="text-right">
                                      <span class="label label-lg label-light-success label-inline">Effectuée</span>
                                    </td>
                                    <td class="text-right pr-0">
                                      <a href="#" class="btn btn-icon btn-light btn-hover-primary btn-sm">
                                        <span class="svg-icon svg-icon-md svg-icon-primary">
                                          <!--begin::Svg Icon | path:assets/media/svg/icons/General/Settings-1.svg-->
                                          <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                              <rect x="0" y="0" width="24" height="24"></rect>
                                              <path d="M7,3 L17,3 C19.209139,3 21,4.790861 21,7 C21,9.209139 19.209139,11 17,11 L7,11 C4.790861,11 3,9.209139 3,7 C3,4.790861 4.790861,3 7,3 Z M7,9 C8.1045695,9 9,8.1045695 9,7 C9,5.8954305 8.1045695,5 7,5 C5.8954305,5 5,5.8954305 5,7 C5,8.1045695 5.8954305,9 7,9 Z" fill="#000000"></path>
                                              <path d="M7,13 L17,13 C19.209139,13 21,14.790861 21,17 C21,19.209139 19.209139,21 17,21 L7,21 C4.790861,21 3,19.209139 3,17 C3,14.790861 4.790861,13 7,13 Z M17,19 C18.1045695,19 19,18.1045695 19,17 C19,15.8954305 18.1045695,15 17,15 C15.8954305,15 15,15.8954305 15,17 C15,18.1045695 15.8954305,19 17,19 Z" fill="#000000" opacity="0.3"></path>
                                            </g>
                                          </svg>
                                          <!--end::Svg Icon-->
                                        </span>
                                      </a>
                                    </td>
                                  </tr>
                                @endforeach
                              </tbody>
                            </table>
                          </div>
                          <!--end::Table-->
                        </div>
                        <!--end::Tap pane-->
                      </div>
                    </div>
                    <!--end::Body-->
                  </div>
                  <!--end::Advance Table Widget 2-->
                </div>
                {{-- <div class="col-lg-1"></div> --}}
              </div>
              <!--end::Row-->

            </div>
            <!--end::Content-->
          </div>
          <!--begin::Content Wrapper-->
        </div>
      @endsection
