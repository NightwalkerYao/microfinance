@extends('layouts.dashboard')

@section('title')
  Tableau de bord
@endsection

@section('content')
  <div class="d-flex flex-row flex-column-fluid container">
    <!--begin::Content Wrapper-->
    <div class="main d-flex flex-column flex-row-fluid">
      <!--begin::Subheader-->
      <div class="subheader py-2 py-lg-6" id="kt_subheader">
        <div class="w-100 d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
          <!--begin::Info-->
          <div class="d-flex align-items-center flex-wrap mr-1">
            <!--begin::Page Heading-->
            <div class="d-flex align-items-baseline flex-wrap mr-5">
              <!--begin::Page Title-->
              <h5 class="text-dark font-weight-bold my-1 mr-5">Tableau de bord</h5>
              <!--end::Page Title-->
              <!--begin::Breadcrumb-->
              <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                <li class="breadcrumb-item">
                  <a href="{{ route('dashboard') }}" class="text-muted">MicroFinance</a>
                </li>
              </ul>
              <!--end::Breadcrumb-->
            </div>
            <!--end::Page Heading-->
          </div>
          <!--end::Info-->
          <!--begin::Toolbar-->
          <div class="d-flex align-items-center">
            <!--begin::Daterange-->
            <a href="#" class="btn btn-light-primary btn-sm font-weight-bold mr-2">
              <span class="opacity-60 font-weight-bold mr-2" id="kt_dashboard_daterangepicker_title">Aujourd'hui :</span>
              <span class="font-weight-bold" id="kt_dashboard_daterangepicker_date">{{ Carbon\Carbon::parse(date('Y-m-d'))->locale('fr')->isoFormat("DD MMM Y") }}</span>
            </a>
            <!--end::Daterange-->
          </div>
          <!--end::Toolbar-->
        </div>
      </div>
      <!--end::Subheader-->

      <!--begin::Content Wrapper-->
      <div class="content flex-column-fluid" id="kt_content">
        <!--begin::Statistics-->
        <div class="d-flex flex-sm-row flex-column">
          <!--begin::Aside-->
          <div class="flex-md-row-auto w-sm-250px w-md-275px w-xl-325px">
            <!--begin::Nav Panel Widget 3-->
            <div class="card card-custom gutter-b">
              <!--begin::Body-->
              <div class="card-body">
                <!--begin::Wrapper-->
                <div class="d-flex justify-content-between flex-column h-100">
                  <!--begin::Container-->
                  <div class="h-100">
                    <!--begin::Header-->
                    <div class="d-flex flex-column flex-center">
                      <!--begin::Image-->
                      <div class="bgi-no-repeat bgi-size-cover rounded min-h-180px w-100" style="background-image: url(/assets/media/stock-600x400/img-70.jpg)"></div>
                      <!--end::Image-->
                      <!--begin::Title-->
                      <a href="{{ url('/') }}" class="card-title font-weight-bolder text-dark-75 text-hover-primary font-size-h4 m-0 pt-7 pb-1">MicroFinance Ivoire</a>
                      <!--end::Title-->
                      <!--begin::Text-->
                      <div class="font-weight-bold text-dark-50 font-size-sm pb-7">KM4 Bvd de Marseille, Treichville</div>
                      <!--end::Text-->
                    </div>
                    <!--end::Header-->
                    <!--begin::Body-->
                    <div class="pt-1">
                      <!--begin::Item-->
                      <div class="d-flex align-items-center pb-9">
                        <!--begin::Symbol-->
                        <div class="symbol symbol-45 symbol-light mr-4">
                          <span class="symbol-label">
                            <span class="svg-icon svg-icon-2x svg-icon-dark-50">
                              <!--begin::Svg Icon | path:assets/media/svg/icons/Media/Equalizer.svg-->
                              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                  <rect x="0" y="0" width="24" height="24" />
                                  <rect fill="#000000" opacity="0.3" x="13" y="4" width="3" height="16" rx="1.5" />
                                  <rect fill="#000000" x="8" y="9" width="3" height="11" rx="1.5" />
                                  <rect fill="#000000" x="18" y="11" width="3" height="9" rx="1.5" />
                                  <rect fill="#000000" x="3" y="13" width="3" height="7" rx="1.5" />
                                </g>
                              </svg>
                              <!--end::Svg Icon-->
                            </span>
                          </span>
                        </div>
                        <!--end::Symbol-->
                        <!--begin::Text-->
                        <div class="d-flex flex-column flex-grow-1">
                          <a href="{{ route('list-agences') }}" class="text-dark-75 text-hover-primary mb-1 font-size-lg font-weight-bolder">Agences</a>
                          <span class="text-muted font-weight-bold">Agences ouvertes</span>
                        </div>
                        <!--end::Text-->
                        <!--begin::label-->
                        <span class="font-weight-bolder label label-xl label-light-success label-inline px-3 py-5 min-w-45px">{{ App\Models\Agence::count() }}</span>
                        <!--end::label-->
                      </div>
                      <!--end::Item-->
                      <!--begin::Item-->
                      <div class="d-flex align-items-center pb-9">
                        <!--begin::Symbol-->
                        <div class="symbol symbol-45 symbol-light mr-4">
                          <span class="symbol-label">
                            <span class="svg-icon svg-icon-2x svg-icon-dark-50">
                              <!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Group.svg-->
                              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                  <polygon points="0 0 24 0 24 24 0 24" />
                                  <path d="M18,14 C16.3431458,14 15,12.6568542 15,11 C15,9.34314575 16.3431458,8 18,8 C19.6568542,8 21,9.34314575 21,11 C21,12.6568542 19.6568542,14 18,14 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                  <path d="M17.6011961,15.0006174 C21.0077043,15.0378534 23.7891749,16.7601418 23.9984937,20.4 C24.0069246,20.5466056 23.9984937,21 23.4559499,21 L19.6,21 C19.6,18.7490654 18.8562935,16.6718327 17.6011961,15.0006174 Z M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
                                </g>
                              </svg>
                              <!--end::Svg Icon-->
                            </span>
                          </span>
                        </div>
                        <!--end::Symbol-->
                        <!--begin::Text-->
                        <div class="d-flex flex-column flex-grow-1">
                          <a href="{{ route('list-clients') }}" class="text-dark-75 text-hover-primary mb-1 font-size-lg font-weight-bolder">Clients</a>
                          <span class="text-muted font-weight-bold">Clients actifs</span>
                        </div>
                        <!--end::Text-->
                        <!--begin::label-->
                        <span class="font-weight-bolder label label-xl label-light-danger label-inline px-3 py-5 min-w-45px">{{ App\Models\Client::count() }}</span>
                        <!--end::label-->
                      </div>
                      <!--end::Item-->
                      <!--begin::Item-->
                      <div class="d-flex align-items-center pb-9">
                        <!--begin::Symbol-->
                        <div class="symbol symbol-45 symbol-light mr-4">
                          <span class="symbol-label">
                            <span class="svg-icon svg-icon-2x svg-icon-dark-50">
                              <!--begin::Svg Icon | path:assets/media/svg/icons/Home/Globe.svg-->
                              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                  <rect x="0" y="0" width="24" height="24" />
                                  <path d="M13,18.9450712 L13,20 L14,20 C15.1045695,20 16,20.8954305 16,22 L8,22 C8,20.8954305 8.8954305,20 10,20 L11,20 L11,18.9448245 C9.02872877,18.7261967 7.20827378,17.866394 5.79372555,16.5182701 L4.73856106,17.6741866 C4.36621808,18.0820826 3.73370941,18.110904 3.32581341,17.7385611 C2.9179174,17.3662181 2.88909597,16.7337094 3.26143894,16.3258134 L5.04940685,14.367122 C5.46150313,13.9156769 6.17860937,13.9363085 6.56406875,14.4106998 C7.88623094,16.037907 9.86320756,17 12,17 C15.8659932,17 19,13.8659932 19,10 C19,7.73468744 17.9175842,5.65198725 16.1214335,4.34123851 C15.6753081,4.01567657 15.5775721,3.39010038 15.903134,2.94397499 C16.228696,2.49784959 16.8542722,2.4001136 17.3003976,2.72567554 C19.6071362,4.40902808 21,7.08906798 21,10 C21,14.6325537 17.4999505,18.4476269 13,18.9450712 Z" fill="#000000" fill-rule="nonzero" />
                                  <circle fill="#000000" opacity="0.3" cx="12" cy="10" r="6" />
                                </g>
                              </svg>
                              <!--end::Svg Icon-->
                            </span>
                          </span>
                        </div>
                        <!--end::Symbol-->
                        <!--begin::Text-->
                        <div class="d-flex flex-column flex-grow-1">
                          <a href="{{ route('list-comptes') }}" class="text-dark-75 text-hover-primary mb-1 font-size-lg font-weight-bolder">Comptes</a>
                          <span class="text-muted font-weight-bold">Comptes actifs</span>
                        </div>
                        <!--end::Text-->
                        <!--begin::label-->
                        <span class="font-weight-bolder label label-xl label-light-primary label-inline py-5 min-w-45px">{{ App\Models\Compte::where('date_fermeture', null)->count() }}</span>
                        <!--end::label-->
                      </div>
                      <!--end::Item-->
                      <!--begin::Item-->
                      <div class="d-flex align-items-center pb-9">
                        <!--begin::Symbol-->
                        <div class="symbol symbol-45 symbol-light mr-4">
                          <span class="symbol-label">
                            <span class="svg-icon svg-icon-2x svg-icon-dark-50">
                              <!--begin::Svg Icon | path:assets/media/svg/icons/Layout/Layout-4-blocks.svg-->
                              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                  <rect x="0" y="0" width="24" height="24" />
                                  <rect fill="#000000" x="4" y="4" width="7" height="7" rx="1.5" />
                                  <path d="M5.5,13 L9.5,13 C10.3284271,13 11,13.6715729 11,14.5 L11,18.5 C11,19.3284271 10.3284271,20 9.5,20 L5.5,20 C4.67157288,20 4,19.3284271 4,18.5 L4,14.5 C4,13.6715729 4.67157288,13 5.5,13 Z M14.5,4 L18.5,4 C19.3284271,4 20,4.67157288 20,5.5 L20,9.5 C20,10.3284271 19.3284271,11 18.5,11 L14.5,11 C13.6715729,11 13,10.3284271 13,9.5 L13,5.5 C13,4.67157288 13.6715729,4 14.5,4 Z M14.5,13 L18.5,13 C19.3284271,13 20,13.6715729 20,14.5 L20,18.5 C20,19.3284271 19.3284271,20 18.5,20 L14.5,20 C13.6715729,20 13,19.3284271 13,18.5 L13,14.5 C13,13.6715729 13.6715729,13 14.5,13 Z" fill="#000000" opacity="0.3" />
                                </g>
                              </svg>
                              <!--end::Svg Icon-->
                            </span>
                          </span>
                        </div>
                        <!--end::Symbol-->
                        <!--begin::Text-->
                        <div class="d-flex flex-column flex-grow-1">
                          <a href="{{ route('list-operations') }}" class="text-dark-75 text-hover-primary mb-1 font-size-lg font-weight-bolder">Opérations</a>
                          <span class="text-muted font-weight-bold">Dépôts, Retraits, ...</span>
                        </div>
                        <!--end::Text-->
                        <!--begin::label-->
                        <span class="font-weight-bolder label label-xl label-light-info label-inline px-3 py-5 min-w-45px">{{ App\Models\Operation::count() }}</span>
                        <!--end::label-->
                      </div>
                      <!--end::Item-->
                    </div>
                    <!--end::Body-->
                  </div>
                  <!--eng::Container-->
                  <div class="d-flex flex-center">
                    <a data-href="{{ route('agios') }}" onclick="$(this).hiddenPost();" class="confirm btn btn-primary font-weight-bolder font-size-sm py-3 px-14" data-confirm="Tous les comptes seront débités de 1000 FCFA. Souhaitez-vous continuer ?"> Prélèvement AGIOS </a>
                  </div>
                </div>
                <!--end::Wrapper-->
              </div>
              <!--end::Body-->
            </div>
            <!--end::Nav Panel Widget 3-->

            <!--begin::List Widget 9-->
            <div class="card card-custom gutter-b">
              <!--begin::Header-->
              <div class="card-header align-items-center border-0 mt-4">
                <h3 class="card-title align-items-start flex-column">
                  <span class="font-weight-bolder text-dark">Activités récentes</span>
                  <span class="text-muted mt-3 font-weight-bold font-size-sm">890 éléments</span>
                </h3>
              </div>
              <!--end::Header-->
              <!--begin::Body-->
              <div class="card-body pt-4">
                <!--begin::Timeline-->
                <div class="timeline timeline-6 mt-3">
                  <!--begin::Item-->
                  <div class="timeline-item align-items-start">
                    <!--begin::Label-->
                    <div class="timeline-label font-weight-bolder text-dark-75 font-size-lg">08:42</div>
                    <!--end::Label-->
                    <!--begin::Badge-->
                    <div class="timeline-badge">
                      <i class="fa fa-genderless text-warning icon-xl"></i>
                    </div>
                    <!--end::Badge-->
                    <!--begin::Text-->
                    <div class="font-weight-mormal font-size-lg timeline-content text-muted pl-3">Korinna Gruhn (Caissière) s'est connectée.</div>
                    <!--end::Text-->
                  </div>
                  <!--end::Item-->
                  <!--begin::Item-->
                  <div class="timeline-item align-items-start">
                    <!--begin::Label-->
                    <div class="timeline-label font-weight-bolder text-dark-75 font-size-lg">10:00</div>
                    <!--end::Label-->
                    <!--begin::Badge-->
                    <div class="timeline-badge">
                      <i class="fa fa-genderless text-success icon-xl"></i>
                    </div>
                    <!--end::Badge-->
                    <!--begin::Content-->
                    <div class="timeline-content d-flex">
                      <span class="font-weight-bolder text-dark-75 pl-3 font-size-lg">Vérifications de routines.</span>
                    </div>
                    <!--end::Content-->
                  </div>
                  <!--end::Item-->
                  <!--begin::Item-->
                  <div class="timeline-item align-items-start">
                    <!--begin::Label-->
                    <div class="timeline-label font-weight-bolder text-dark-75 font-size-lg">14:37</div>
                    <!--end::Label-->
                    <!--begin::Badge-->
                    <div class="timeline-badge">
                      <i class="fa fa-genderless text-danger icon-xl"></i>
                    </div>
                    <!--end::Badge-->
                    <!--begin::Desc-->
                    <div class="timeline-content font-weight-bolder font-size-lg text-dark-75 pl-3">
                      Dépôt effectué
                      <a href="javascript:;" class="text-primary">72 000 FCFA</a> à #MF-01674889
                    </div>
                    <!--end::Desc-->
                  </div>
                  <!--end::Item-->
                  <!--begin::Item-->
                  <div class="timeline-item align-items-start">
                    <!--begin::Label-->
                    <div class="timeline-label font-weight-bolder text-dark-75 font-size-lg">16:50</div>
                    <!--end::Label-->
                    <!--begin::Badge-->
                    <div class="timeline-badge">
                      <i class="fa fa-genderless text-primary icon-xl"></i>
                    </div>
                    <!--end::Badge-->
                    <!--begin::Text-->
                    <div class="timeline-content font-weight-mormal font-size-lg text-muted pl-3">La fonctionnalité "Emprunts et Remboursements" a été désactivé.</div>
                    <!--end::Text-->
                  </div>
                  <!--end::Item-->
                  <!--begin::Item-->
                  <div class="timeline-item align-items-start">
                    <!--begin::Label-->
                    <div class="timeline-label font-weight-bolder text-dark-75 font-size-lg">21:03</div>
                    <!--end::Label-->
                    <!--begin::Badge-->
                    <div class="timeline-badge">
                      <i class="fa fa-genderless text-danger icon-xl"></i>
                    </div>
                    <!--end::Badge-->
                    <!--begin::Desc-->
                    <div class="timeline-content font-weight-bolder text-dark-75 pl-3 font-size-lg">
                      Retrait en espèce
                      <a href="javascript:;" class="text-primary">30 000FCFA</a> sur #MF-17067538.
                    </div>
                    <!--end::Desc-->
                  </div>
                  <!--end::Item-->
                  <!--begin::Item-->
                  <div class="timeline-item align-items-start">
                    <!--begin::Label-->
                    <div class="timeline-label font-weight-bolder text-dark-75 font-size-lg">23:07</div>
                    <!--end::Label-->
                    <!--begin::Badge-->
                    <div class="timeline-badge">
                      <i class="fa fa-genderless text-info icon-xl"></i>
                    </div>
                    <!--end::Badge-->
                    <!--begin::Text-->
                    <div class="timeline-content font-weight-mormal font-size-lg text-muted pl-3">
                      Vous avez programmé une session de maintenance pour le 30 juin, 02:00
                    </div>
                    <!--end::Text-->
                  </div>
                  <!--end::Item-->
                </div>
                <!--end::Timeline-->
              </div>
              <!--end: Card Body-->
            </div>
            <!--end: List Widget 9-->
          </div>
          <!--end::Aside-->
          <!--begin::Content-->
          <div class="flex-row-fluid ml-sm-8">
            <div class="row">
              <div class="col-xxl-6">
                <!--begin::Mixed Widget 2-->
                <div class="card card-custom bg-gray-100 gutter-b">
                  <!--begin::Header-->
                  <div class="card-header border-0 bg-primary py-5">
                    <h3 class="card-title font-weight-bolder text-white">Opérations</h3>
                    <div class="card-toolbar">
                      <div class="dropdown dropdown-inline">
                        <a href="#" class="btn btn-transparent-white btn-sm font-weight-bolder dropdown-toggle px-5" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Exporter</a>
                        <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                          <!--begin::Navigation-->
                          <ul class="navi navi-hover">
                            <li class="navi-header pb-1">
                              <span class="text-primary text-uppercase font-weight-bold font-size-sm">Historiques :</span>
                            </li>
                            <li class="navi-item">
                              <a href="#" class="navi-link">
                                <span class="navi-icon">
                                  <i class="flaticon2-shopping-cart-1"></i>
                                </span>
                                <span class="navi-text">Retraits</span>
                              </a>
                            </li>
                            <li class="navi-item">
                              <a href="#" class="navi-link">
                                <span class="navi-icon">
                                  <i class="flaticon2-calendar-8"></i>
                                </span>
                                <span class="navi-text">Dépôts</span>
                              </a>
                            </li>
                            <li class="navi-item">
                              <a href="#" class="navi-link">
                                <span class="navi-icon">
                                  <i class="flaticon2-graph-1"></i>
                                </span>
                                <span class="navi-text">Transferts</span>
                              </a>
                            </li>
                            <li class="navi-item">
                              <a href="#" class="navi-link">
                                <span class="navi-icon">
                                  <i class="flaticon2-rocket-1"></i>
                                </span>
                                <span class="navi-text">Tout</span>
                              </a>
                            </li>
                          </ul>
                          <!--end::Navigation-->
                        </div>
                      </div>
                    </div>
                  </div>
                  <!--end::Header-->
                  <!--begin::Body-->
                  <div class="card-body p-0 position-relative overflow-hidden">
                    <!--begin::Chart-->
                    <div id="kt_mixed_widget_2_chart" class="card-rounded-bottom bg-primary" style="height: 200px"></div>
                    <!--end::Chart-->
                    <!--begin::Stats-->
                    <div class="card-spacer mt-n25">
                      <!--begin::Row-->
                      <div class="row m-0">
                        <div class="col bg-white px-6 py-8 rounded-xl mr-7 mb-7">
                          <span class="svg-icon svg-icon-3x svg-icon-info d-block my-2">
                            <!--begin::Svg Icon | path:assets/media/svg/icons/Media/Equalizer.svg-->
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                              <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24" />
                                <rect fill="#000000" opacity="0.3" x="13" y="4" width="3" height="16" rx="1.5" />
                                <rect fill="#000000" x="8" y="9" width="3" height="11" rx="1.5" />
                                <rect fill="#000000" x="18" y="11" width="3" height="9" rx="1.5" />
                                <rect fill="#000000" x="3" y="13" width="3" height="7" rx="1.5" />
                              </g>
                            </svg>
                            <!--end::Svg Icon-->
                          </span>
                          <a href="{{ route('create-agence') }}" class="text-info font-weight-bold font-size-h6">Créer Agence</a>
                        </div>
                        <div class="col bg-white px-6 py-8 rounded-xl mb-7">
                          <span class="svg-icon svg-icon-3x svg-icon-warning d-block my-2">
                            <!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                              <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <polygon points="0 0 24 0 24 24 0 24" />
                                <path d="M18,8 L16,8 C15.4477153,8 15,7.55228475 15,7 C15,6.44771525 15.4477153,6 16,6 L18,6 L18,4 C18,3.44771525 18.4477153,3 19,3 C19.5522847,3 20,3.44771525 20,4 L20,6 L22,6 C22.5522847,6 23,6.44771525 23,7 C23,7.55228475 22.5522847,8 22,8 L20,8 L20,10 C20,10.5522847 19.5522847,11 19,11 C18.4477153,11 18,10.5522847 18,10 L18,8 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                <path d="M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
                              </g>
                            </svg>
                            <!--end::Svg Icon-->
                          </span>
                          <a href="{{ route('create-client') }}" class="text-warning font-weight-bold font-size-h6 mt-2">Ajouter Client</a>
                        </div>
                      </div>
                      <!--end::Row-->
                      <!--begin::Row-->
                      <div class="row m-0">
                        <div class="col bg-white px-6 py-8 rounded-xl mr-7">
                          <span class="svg-icon svg-icon-3x svg-icon-danger d-block my-2">
                            <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Layers.svg-->
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                              <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <polygon points="0 0 24 0 24 24 0 24" />
                                <path d="M12.9336061,16.072447 L19.36,10.9564761 L19.5181585,10.8312381 C20.1676248,10.3169571 20.2772143,9.3735535 19.7629333,8.72408713 C19.6917232,8.63415859 19.6104327,8.55269514 19.5206557,8.48129411 L12.9336854,3.24257445 C12.3871201,2.80788259 11.6128799,2.80788259 11.0663146,3.24257445 L4.47482784,8.48488609 C3.82645598,9.00054628 3.71887192,9.94418071 4.23453211,10.5925526 C4.30500305,10.6811601 4.38527899,10.7615046 4.47382636,10.8320511 L4.63,10.9564761 L11.0659024,16.0730648 C11.6126744,16.5077525 12.3871218,16.5074963 12.9336061,16.072447 Z" fill="#000000" fill-rule="nonzero" />
                                <path d="M11.0563554,18.6706981 L5.33593024,14.122919 C4.94553994,13.8125559 4.37746707,13.8774308 4.06710397,14.2678211 C4.06471678,14.2708238 4.06234874,14.2738418 4.06,14.2768747 L4.06,14.2768747 C3.75257288,14.6738539 3.82516916,15.244888 4.22214834,15.5523151 C4.22358765,15.5534297 4.2250303,15.55454 4.22647627,15.555646 L11.0872776,20.8031356 C11.6250734,21.2144692 12.371757,21.2145375 12.909628,20.8033023 L19.7677785,15.559828 C20.1693192,15.2528257 20.2459576,14.6784381 19.9389553,14.2768974 C19.9376429,14.2751809 19.9363245,14.2734691 19.935,14.2717619 L19.935,14.2717619 C19.6266937,13.8743807 19.0546209,13.8021712 18.6572397,14.1104775 C18.654352,14.112718 18.6514778,14.1149757 18.6486172,14.1172508 L12.9235044,18.6705218 C12.377022,19.1051477 11.6029199,19.1052208 11.0563554,18.6706981 Z" fill="#000000" opacity="0.3" />
                              </g>
                            </svg>
                            <!--end::Svg Icon-->
                          </span>
                          <a href="{{ route('create-compte') }}" class="text-danger font-weight-bold font-size-h6 mt-2">Créer Compte</a>
                        </div>
                        <div class="col bg-white px-6 py-8 rounded-xl">
                          <span class="svg-icon svg-icon-3x svg-icon-success d-block my-2">
                            <!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Urgent-mail.svg-->
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                              <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24" />
                                <path d="M12.7037037,14 L15.6666667,10 L13.4444444,10 L13.4444444,6 L9,12 L11.2222222,12 L11.2222222,14 L6,14 C5.44771525,14 5,13.5522847 5,13 L5,3 C5,2.44771525 5.44771525,2 6,2 L18,2 C18.5522847,2 19,2.44771525 19,3 L19,13 C19,13.5522847 18.5522847,14 18,14 L12.7037037,14 Z" fill="#000000" opacity="0.3" />
                                <path d="M9.80428954,10.9142091 L9,12 L11.2222222,12 L11.2222222,16 L15.6666667,10 L15.4615385,10 L20.2072547,6.57253826 C20.4311176,6.4108595 20.7436609,6.46126971 20.9053396,6.68513259 C20.9668779,6.77033951 21,6.87277228 21,6.97787787 L21,17 C21,18.1045695 20.1045695,19 19,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,6.97787787 C3,6.70173549 3.22385763,6.47787787 3.5,6.47787787 C3.60510559,6.47787787 3.70753836,6.51099993 3.79274528,6.57253826 L9.80428954,10.9142091 Z" fill="#000000" />
                              </g>
                            </svg>
                            <!--end::Svg Icon-->
                          </span>
                          <a href="{{ route('list-operations') }}" class="text-success font-weight-bold font-size-h6 mt-2">Transactions</a>
                        </div>
                      </div>
                      <!--end::Row-->
                    </div>
                    <!--end::Stats-->
                  </div>
                  <!--end::Body-->
                </div>
                <!--end::Mixed Widget 2-->
                <!--begin::Mixed Widget 13-->
                <div class="card card-custom gutter-b">
                  <!--begin::Beader-->
                  <div class="card-header border-0 py-5">
                    <h3 class="card-title font-weight-bolder">{{ Carbon\Carbon::parse(date("Y-m-d"))->locale('fr')->isoFormat("MMMM Y") }}</h3>
                  </div>
                  <!--end::Header-->
                  <!--begin::Body-->
                  <div class="card-body p-0 d-flex flex-column">
                    <!--begin::Stats-->
                    <div class="card-spacer pt-5 bg-white flex-grow-1">
                      <!--begin::Row-->
                      @php
                      $operations = App\Models\Operation::whereMonth("date", date("m"))->whereYear("date", date("Y"))->get();
                      $td = 0;
                      foreach ($operations->where('type_operation_id', 1)  as $o) {
                        $td += $o->montant;
                      }
                      $tr = 0;
                      foreach ($operations->where('type_operation_id', 2)  as $o) {
                        $tr += $o->montant;
                      }
                      $tt = 0;
                      foreach ($operations->where('type_operation_id', 3)  as $o) {
                        $tt += $o->montant;
                      }
                      $t = 0;
                      foreach ($operations  as $o) {
                        $t += $o->montant;
                      }
                      @endphp
                      <div class="row row-paddingless">
                        <div class="col mr-8">
                          <div class="font-size-sm text-muted font-weight-bold">Dépôts</div>
                          <div class="font-size-h4 font-weight-bolder">{{ $td }} FCFA</div>
                        </div>
                        <div class="col">
                          <div class="font-size-sm text-muted font-weight-bold">Retraits</div>
                          <div class="font-size-h4 font-weight-bolder">{{ $tr }} FCFA</div>
                        </div>
                      </div>
                      <!--end::Row-->
                      <!--begin::Row-->
                      <div class="row row-paddingless mt-8">
                        <div class="col mr-8">
                          <div class="font-size-sm text-muted font-weight-bold">Transferts</div>
                          <div class="font-size-h4 font-weight-bolder">{{ $tt }} FCFA</div>
                        </div>
                        <div class="col">
                          <div class="font-size-sm text-muted font-weight-bold">Total</div>
                          <div class="font-size-h4 font-weight-bolder">{{ $t }} FCFA</div>
                        </div>
                      </div>
                      <!--end::Row-->
                    </div>
                    <!--end::Stats-->
                    <!--begin::Chart-->
                    <div id="kt_mixed_widget_13_chart" class="card-rounded-bottom" style="height: 200px"></div>
                    <!--end::Chart-->
                  </div>
                  <!--end::Body-->
                </div>
                <!--end::Mixed Widget 13-->
              </div>
              <div class="col-xxl-6">
                <!--begin::Mixed Widget 7-->
                <div class="card card-custom gutter-b">
                  <!--begin::Body-->
                  <div class="card-body">
                    <div class="d-flex flex-wrap align-items-center py-1">
                      <!--begin:Pic-->
                      <div class="symbol symbol-80 symbol-light-danger mr-5">
                        <span class="symbol-label">
                          <img src="assets/media/svg/misc/008-infography.svg" class="h-50 align-self-center" alt="" />
                        </span>
                      </div>
                      <!--end:Pic-->
                      <!--begin:Title-->
                      <div class="d-flex flex-column flex-grow-1 my-lg-0 my-2 pr-3">
                        <a href="{{ route('list-clients') }}" class="text-dark font-weight-bolder text-hover-primary font-size-h5">
                          Nouveaux Clients par
                          <br />
                          rapport à {{ Carbon\Carbon::parse(date("Y-m-d", strtotime("previous month")))->locale('fr')->isoFormat("MMMM Y") }}
                        </a>
                        <span class="text-muted font-weight-bold font-size-lg">Fin : {{ Carbon\Carbon::parse(date("Y-m-t"))->locale('fr')->isoFormat("dddd DD MMM Y") }}</span>
                      </div>
                      <!--end:Title-->
                      <!--begin:Stats-->
                      <div class="d-flex flex-column w-100 mt-12">
                        <span class="text-dark mr-2 font-size-lg font-weight-bolder pb-3">Progression</span>
                        <div class="progress progress-xs w-100">
                          <div class="progress-bar bg-danger" role="progressbar" style="width: 45%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                      </div>
                      <!--end:Stats-->
                      <!--begin:Team-->
                      <div class="d-flex flex-column mt-10">
                        <div class="text-dark mr-2 font-size-lg font-weight-bolder pb-4">Derniers Clients</div>
                        <div class="d-flex">
                          @foreach (App\Models\Client::orderByDesc('id')->limit(6)->get() as $client)
                            <a href="{{ route('show-client', ['client' => $client->id]) }}" class="symbol symbol-50 flex-shrink-0 mr-3" data-toggle="tooltip" data-placement="top" title="{{ $client->nom_complet }}">
                              <div class="symbol-label" style="background-image: url('{{ $client->photo_path }}')"></div>
                            </a>
                          @endforeach
                        </div>
                      </div>
                      <!--end:Team-->
                    </div>
                  </div>
                  <!--end::Body-->
                </div>
                <!--end::Mixed Widget 7-->
                <!--begin::Mixed Widget 18-->
                <div class="card card-custom gutter-b">
                  <!--begin::Header-->
                  <div class="card-header border-0 pt-5">
                    <div class="card-title font-weight-bolder">
                      <div class="card-label">Cette semaine
                        <div class="font-size-sm text-muted mt-2">
                          {{ App\Models\Operation::where('date', ">=", date("Y-m-d", strtotime("-7 days")))->count() }} Opérations
                        </div></div>
                      </div>
                    </div>
                    <!--end::Header-->
                    <!--begin::Body-->
                    <div class="card-body">
                      <!--begin::Chart-->
                      <div id="kt_mixed_widget_18_chart" style="height: 250px"></div>
                      <!--end::Chart-->
                      <!--begin::Items-->
                      <div class="mt-n12 position-relative zindex-0">
                        <!--begin::Widget Item-->
                        <div class="d-flex align-items-center mb-8">
                          <!--begin::Symbol-->
                          <div class="symbol symbol-circle symbol-40 symbol-light mr-3 flex-shrink-0">
                            <div class="symbol-label">
                              <span class="svg-icon svg-icon-lg svg-icon-gray-500">
                                <!--begin::Svg Icon | path:assets/media/svg/icons/Media/Equalizer.svg-->
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                  <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <rect x="0" y="0" width="24" height="24" />
                                    <rect fill="#000000" opacity="0.3" x="13" y="4" width="3" height="16" rx="1.5" />
                                    <rect fill="#000000" x="8" y="9" width="3" height="11" rx="1.5" />
                                    <rect fill="#000000" x="18" y="11" width="3" height="9" rx="1.5" />
                                    <rect fill="#000000" x="3" y="13" width="3" height="7" rx="1.5" />
                                  </g>
                                </svg>
                                <!--end::Svg Icon-->
                              </span>
                            </div>
                          </div>
                          <!--end::Symbol-->
                          <!--begin::Title-->
                          <div>
                            <a href="#" class="font-size-h6 text-dark-75 text-hover-primary font-weight-bolder">{{ auth()->user()->name }}</a>
                            <div class="font-size-sm text-muted font-weight-bold mt-1">Client ayant fait le plus d'opérations</div>
                          </div>
                          <!--end::Title-->
                        </div>
                        <!--end::Widget Item-->
                        <!--begin::Widget Item-->
                        <div class="d-flex align-items-center mb-8">
                          <!--begin::Symbol-->
                          <div class="symbol symbol-circle symbol-40 symbol-light mr-3 flex-shrink-0">
                            <div class="symbol-label">
                              <span class="svg-icon svg-icon-lg svg-icon-gray-500">
                                <!--begin::Svg Icon | path:assets/media/svg/icons/Shopping/Chart-pie.svg-->
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                  <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <rect x="0" y="0" width="24" height="24" />
                                    <path d="M4.00246329,12.2004927 L13,14 L13,4.06189375 C16.9463116,4.55399184 20,7.92038235 20,12 C20,16.418278 16.418278,20 12,20 C7.64874861,20 4.10886412,16.5261253 4.00246329,12.2004927 Z" fill="#000000" opacity="0.3" />
                                    <path d="M3.0603968,10.0120794 C3.54712466,6.05992157 6.91622084,3 11,3 L11,11.6 L3.0603968,10.0120794 Z" fill="#000000" />
                                  </g>
                                </svg>
                                <!--end::Svg Icon-->
                              </span>
                            </div>
                          </div>
                          <!--end::Symbol-->
                          <!--begin::Title-->
                          <div>
                            <a href="#" class="font-size-h6 text-dark-75 text-hover-primary font-weight-bolder">Total des transactions</a>
                            <div class="font-size-sm text-muted font-weight-bold mt-1">+40% par rapport à la semaine précédente</div>
                          </div>
                          <!--end::Title-->
                        </div>
                        <!--end::Widget Item-->
                        <!--begin::Widget Item-->
                        <div class="d-flex align-items-center">
                          <!--begin::Symbol-->
                          <div class="symbol symbol-circle symbol-40 symbol-light mr-3 flex-shrink-0">
                            <div class="symbol-label">
                              <span class="svg-icon svg-icon-lg svg-icon-gray-500">
                                <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Layers.svg-->
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                  <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <polygon points="0 0 24 0 24 24 0 24" />
                                    <path d="M12.9336061,16.072447 L19.36,10.9564761 L19.5181585,10.8312381 C20.1676248,10.3169571 20.2772143,9.3735535 19.7629333,8.72408713 C19.6917232,8.63415859 19.6104327,8.55269514 19.5206557,8.48129411 L12.9336854,3.24257445 C12.3871201,2.80788259 11.6128799,2.80788259 11.0663146,3.24257445 L4.47482784,8.48488609 C3.82645598,9.00054628 3.71887192,9.94418071 4.23453211,10.5925526 C4.30500305,10.6811601 4.38527899,10.7615046 4.47382636,10.8320511 L4.63,10.9564761 L11.0659024,16.0730648 C11.6126744,16.5077525 12.3871218,16.5074963 12.9336061,16.072447 Z" fill="#000000" fill-rule="nonzero" />
                                    <path d="M11.0563554,18.6706981 L5.33593024,14.122919 C4.94553994,13.8125559 4.37746707,13.8774308 4.06710397,14.2678211 C4.06471678,14.2708238 4.06234874,14.2738418 4.06,14.2768747 L4.06,14.2768747 C3.75257288,14.6738539 3.82516916,15.244888 4.22214834,15.5523151 C4.22358765,15.5534297 4.2250303,15.55454 4.22647627,15.555646 L11.0872776,20.8031356 C11.6250734,21.2144692 12.371757,21.2145375 12.909628,20.8033023 L19.7677785,15.559828 C20.1693192,15.2528257 20.2459576,14.6784381 19.9389553,14.2768974 C19.9376429,14.2751809 19.9363245,14.2734691 19.935,14.2717619 L19.935,14.2717619 C19.6266937,13.8743807 19.0546209,13.8021712 18.6572397,14.1104775 C18.654352,14.112718 18.6514778,14.1149757 18.6486172,14.1172508 L12.9235044,18.6705218 C12.377022,19.1051477 11.6029199,19.1052208 11.0563554,18.6706981 Z" fill="#000000" opacity="0.3" />
                                  </g>
                                </svg>
                                <!--end::Svg Icon-->
                              </span>
                            </div>
                          </div>
                          <!--end::Symbol-->
                          <!--begin::Title-->
                          <div>
                            <a href="#" class="font-size-h6 text-dark-75 text-hover-primary font-weight-bolder">{{ App\Models\Agence::first()->libelle }}</a>
                            <div class="font-size-sm text-muted font-weight-bold mt-1">Meilleure Agence en termes d'opérations</div>
                          </div>
                          <!--end::Title-->
                        </div>
                        <!--end::Widget Item-->
                      </div>
                      <!--end::Items-->
                    </div>
                    <!--end::Body-->
                  </div>
                  <!--end::Mixed Widget 18-->
                  <!--begin::Stats Widget 7-->
                  <div class="card card-custom gutter-b">
                    <!--begin::Body-->
                    <div class="card-body d-flex flex-column p-0">
                      <div class="d-flex align-items-center justify-content-between card-spacer flex-grow-1">
                        <div class="d-flex flex-column mr-2">
                          <a href="{{ route('list-operations') }}" class="text-dark-75 text-hover-primary font-weight-bolder font-size-h5">Dépôts de cette semaine</a>
                          <span class="text-muted font-weight-bold mt-2">Evolution des opérations</span>
                        </div>
                        <span class="symbol symbol-light-success symbol-45">
                          <span class="symbol-label font-weight-bolder font-size-h6">+{{ App\Models\Operation::where('date', ">=", date("Y-m-d", strtotime("-7 days")))->where('type_operation_id', 1)->count() }}</span>
                        </span>
                      </div>
                      <div id="kt_stats_widget_7_chart" class="card-rounded-bottom" style="height: 150px"></div>
                    </div>
                    <!--end::Body-->
                  </div>
                  <!--end::Stats Widget 7-->
                </div>
              </div>
            </div>
            <!--end::Content-->
          </div>
          <!--end::Statistics-->
        </div>
        <!--end::Content-->
      </div>
      <!--begin::Content Wrapper-->
    </div>
    <!--end::Container-->

  @endsection
  @section('specific-js')
    <script src="/assets/js/pages/widgets.js"></script>
  @endsection
