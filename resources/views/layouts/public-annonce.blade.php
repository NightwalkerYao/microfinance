<!doctype html>
<html lang="fr">
  <head>
    {{-- <base href="/"> --}}
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>@section('title') Emploi @show - CGEDS</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('public/assets/css/bootstrap.min.css') }}">

    <!-- External Css -->
    <link rel="stylesheet" href="{{ asset('public/assets/css/fontawesome-all.min.css') }}">
    {{-- <link rel="stylesheet" href="{{ asset('public/assets/css/themify-icons.css') }}" /> --}}
    {{-- <link rel="stylesheet" href="{{ asset('public/assets/css/et-line.css') }}" /> --}}
    {{-- <link rel="stylesheet" href="{{ asset('public/assets/css/bootstrap-select.min.css') }}" /> --}}
    {{-- <link rel="stylesheet" href="{{ asset('public/assets/css/plyr.css') }}" /> --}}
    {{-- <link rel="stylesheet" href="{{ asset('public/assets/css/flag.css') }}" /> --}}
    {{-- <link rel="stylesheet" href="{{ asset('public/assets/css/slick.css') }}" /> --}}
    {{-- <link rel="stylesheet" href="{{ asset('public/assets/css/owl.carousel.min.css') }}" /> --}}
    {{-- <link rel="stylesheet" href="{{ asset('public/assets/css/jquery.nstSlider.min.css') }}" /> --}}

    @yield('extra-css')

    <!-- Custom Css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('public/css/main.css') }}">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600%7CRoboto:300i,400,500" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="/public/assets/js/html5shiv.min.js"></script>
    <script src="/public/assets/js/respond.min.js"></script>
    <![endif]-->

  </head>
  <body>

    @isset($navbar)
      {{-- navbar is hidden --}}
    @else
      <header class="header-2">
        <div class="container">
          <div class="row">
            <div class="col">
              <div class="header-top">
                <div class="logo-area">
                  <a href="/"><img src="/public/images/logo-2-1.png" alt=""></a>
                </div>
              </div>
              <nav class="navbar navbar-expand-lg cp-nav-2">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  <ul class="navbar-nav">
                    <li class="menu-item active"><a href="{{ route('list-annonces-externe') }}">Toutes les annonces</a></li>
                    <li class="menu-item"><a target="_blank" href="https://cgeds.com/contact-2/">Contact</a></li>
                  </ul>
                </div>
              </nav>
            </div>
          </div>
        </div>
      </header>
    @endisset

    @yield('content')

    <!-- Footer -->
    <footer class="footer-bg">
      <div class="footer-bottom-area">
        <div class="container">
          <div class="row">
            <div class="col-sm-12">
              <div class="footer-bottom border-top">
                <div class="row">
                  {{-- <div class="col-xl-4 col-lg-5 order-lg-2"></div> --}}
                  <div class="col-xl-8 col-lg-9 order-lg-1">
                    <p class="copyright-text">
                      2021 &copy; Cabinet de G&eacute;om&egrave;tre Expert Diallo S&eacute;kou - SIRH Propuls&eacute; par
                      <a href="https://quanteex.com/" target="_blank" class="text-dark-50 text-hover-primary">Quanteex</a>
                    </p>
                  </div>
                  <div class="col-xl-4 col-lg-3 order-lg-2">
                    <div class="back-to-top">
                      <a href="#">Retour en haut<i class="fas fa-angle-up"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
    <!-- Footer End -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{ asset("public/assets/js/jquery.min.js") }}"></script>
    <script src="{{ asset('public/assets/js/popper.min.js') }}"></script>
    <script src="{{ asset('public/assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('public/assets/js/feather.min.js') }}"></script>
    {{-- <script src="{{ asset('public/assets/js/bootstrap-select.min.js') }}"></script> --}}
    {{-- <script src="{{ asset('public/assets/js/jquery.nstSlider.min.js') }}"></script> --}}
    {{-- <script src="{{ asset('public/assets/js/owl.carousel.min.js') }}"></script> --}}
    {{-- <script src="{{ asset('public/assets/js/visible.js') }}"></script> --}}
    {{-- <script src="{{ asset('public/assets/js/jquery.countTo.js') }}"></script> --}}
    {{-- <script src="{{ asset('public/assets/js/chart.js') }}"></script> --}}
    {{-- <script src="{{ asset('public/assets/js/plyr.js') }}"></script> --}}
    {{-- <script src="{{ asset('public/assets/js/tinymce.min.js') }}"></script> --}}
    {{-- <script src="{{ asset('public/assets/js/slick.min.js') }}"></script> --}}
    {{-- <script src="{{ asset('public/assets/js/jquery.ajaxchimp.min.js') }}"></script> --}}

    @yield('extra-js')

    <script src="{{ asset('public/js/custom.js') }}"></script>
  </body>
</html>
