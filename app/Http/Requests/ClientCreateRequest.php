<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClientCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'cni' => "required|string|between:5,15",
          'nom' => "required|string|between:2,50",
          'prenom' => "required|string|between:2,100",
          'date_naissance' => "required|date|date_format:Y-m-d|before_or_equal:".date("Y-m-d", strtotime("-16 years"))."|after_or_equal:".date("Y-m-d", strtotime("-90 years")),
          'profession' => "required|string|between:2,64",
          'telephone' => "required|string|between:2,40",
          'sexe_id' => "required|integer|exists:sexes,id",
          'signature_file' => "sometimes|file|image|max:15000",
          'photo_file' => "sometimes|file|image|max:15000",
          'continue' => "sometimes|url",
          'photo_file_remove' => "sometimes|nullable|string",
          'signature_file_remove' => "sometimes|nullable|string",
        ];
    }


    public function messages()
    {
      return [
        'cni.required' => "Le numéro de la CNI est requis.",
        'cni.between' => "Le numéro de la CNI doit faire entre :min et :max caractères.",
        'nom.required' => "Le nom est requis.",
        'nom.between' => "Le nom doit faire entre :min et :max caractères.",
        'prenom.required' => "Le prénom est requis.",
        'prenom.between' => "Le prénom doit faire entre :min et :max caractères.",
        'date_naissance.required' => "La date de naissance est requise.",
        'date_naissance.date' => "La date de naissance n'est pas valide.",
        'date_naissance.date_format' => "Le format de la date de naissance doit être AAAA-MM-JJ",
        'date_naissance.before_or_equal' => "Le Client doit être né avant le :date",
        'date.after_or_equal' => "Le Client doit être né après le :date",
        'profession.required' => "Veuillez indiquer la profession du Client.",
        'profession.between' => "La profession doit faire entre :min et :max caractères.",
        'telephone.required' => "Veuillez indiquer le numéro de téléphone du Client.",
        'telephone.between' => "Le numéro de téléphone doit faire entre :min et :max caractères.",
        'sexe_id.required' => "Veuillez sélectionner le sexe du Client.",
        'sexe_id.exists' => "Le sexe choisi n'est pas valide.",
        'signature_file.file' => "Veuillez choisir le fichier image contenant la signature du Client.",
        'signature_file.image' => "Veuillez choisir le fichier image contenant la signature du Client.",
        'signature_file.max' => "Le fichier image de la signature est trop grand.",
        'photo_file.file' => "Veuillez choisir la photo du Client.",
        'photo_file.image' => "Veuillez choisir la photo du Client.",
        'photo_file.max' => "La photo est trop grande.",
      ];
    }
}
