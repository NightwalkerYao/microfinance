<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddTransfertRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'cni' => "required|string|between:5,15",
            'nom' => "required|string|between:2,50",
            'prenom'=>"required|string|between:2,50",
            "numero_exp"=>"required|string|between:2,50",
            "numero_rec"=>"required|string|between:2,50",
            "montant"=>"required|string",

        ];
    }
    public function messages()
    {
        return [
            'cni.required' => "Le numéro de la CNI est requis.",
            'cni.between' => "Le numéro de la CNI doit faire entre :min et :max caractères.",
            'nom.required' => "Le nom est requis.",
            'nom.between' => "Le nom doit faire entre :min et :max caractères.",
            'prenom.required' => "Le prénom est requis.",
            'prenom.between' => "Le prénom doit faire entre :min et :max caractères.",
            'numero_exp.required' => "Le numero est requis.",
            'numero_exp.between' => "Le numero doit faire entre :min et :max caractères.",
            'montant.required' => "Le montant est requis.",
            'numero_rec.required' => "Le numero est requis.",
            'numero_rec.between' => "Le numero doit faire entre :min et :max caractères.",
            
          ];
    }
}
