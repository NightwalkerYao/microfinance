<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'firstname' => "required|string|between:1,40",
          'lastname' => "required|string|between:1,100",
          'matricule' => "nullable|string|between:1,40",
          'photo_file' => "sometimes|file|image",
          'email' => "required|string|email|unique:users,email",
          'role' => "required|string|in:gerant_adjoint,caissiere",
        ];
    }
}
