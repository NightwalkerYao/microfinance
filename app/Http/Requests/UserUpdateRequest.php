<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'name' => "required|string|between:1,160",
          'matricule' => "nullable|string|between:1,40",
          'photo_file' => "sometimes|file|image",
          'photo_file_remove' => "nullable|string",
          'email' => "required|string|email",
          'password' => "nullable|string|between:3,40",
          'role' => "required|string|in:gerant_adjoint,caissiere,gerant,client",
          'active' => "required|string|in:0,1",
          'continue' => "sometimes|nullable|string",
        ];
    }
}
