<?php

namespace App\Http\Controllers;

use App\Models\Compte;
use App\Models\Client;
use App\Models\Operation;
use Illuminate\Http\Request;
use App\Http\Requests\CompteCreateRequest;
use App\Http\Requests\CompteEditRequest;
use Yajra\DataTables\Facades\DataTables as FacadesDataTables;
use Carbon\Carbon;

class CompteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      if ($request->ajax()) {
        $model = Compte::query();
        return FacadesDataTables::eloquent($model->with(['client']))
        ->filter(function ($query) {
          if (request()->has('query.q')) {
            $query->where('numero', 'like', "%" . request('query.q') . "%")
            ->orWhere('observation', 'like', "%" . request('query.q') . "%");
          }
          if(request()->has('sort.field') && request()->has('sort.sort') && in_array(request('sort.field'), ["DT_RowIndex", "numero", "date_ouverture", "visa_ouverture", "date_fermeture", "visa_fermeture", "solde", "client", "created_at"]) && in_array(request('sort.sort'), ['asc', 'desc'])) {
            $cols = [
              "DT_RowIndex" => "id",
              "numero" => "numero",
              "date_ouverture" => "date_ouverture",
              "visa_ouverture" => "visa_ouverture",
              "date_fermeture" => "date_fermeture",
              "visa_fermeture" => "visa_fermeture",
              "solde" => "solde",
              "client" => "client_id",
              "created_at" => "created_at",
            ];
            $query->orderBy($cols[request("sort.field")], request("sort.sort"));
          } else {
            $query->orderByDesc('id');
          }
        }, true)
        ->addIndexColumn()
        ->editColumn('created_at', function (Compte $c) {
          return Carbon::parse($c->created_at)->locale('fr')->isoFormat("DD MMM YYYY, HH:mm");
        })
        ->editColumn('date_ouverture', function (Compte $c) {
          return Carbon::parse($c->date_ouverture)->locale('fr')->isoFormat("DD/MM/YYYY");
        })
        ->addColumn('action', function ($row) {
          return 'Edit/Delete';
        })
        ->toJson();
      }

      return view("contents.comptes.list", [
        'count' => Compte::count(),
      ]);
    }

    public function jsonList(Request $request)
    {
      return response()->json(
        Client::where('nom', 'like', "%" . request('q') . "%")
          ->orWhere('prenom', 'like', "%" . request('q') . "%")
          ->paginate(30),
      );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('contents.comptes.create', [
        'clients' => Client::all(),
      ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompteCreateRequest $request)
    {
        $input = $request->validated();
        $input['visa_ouverture'] = auth()->user()->name;
        Compte::create($input);

        return response()->json([
          'success' => true,
          'message' => "Le Compte a bien été crée.",
          // 'input' => $input,
          'redirect' => route('list-comptes'),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Compte  $compte
     * @return \Illuminate\Http\Response
     */
    public function show(Compte $compte)
    {
      return view('contents.comptes.show', [
        'compte' => $compte,
      ]);
    }


    public function freeze(Compte $compte)
    {
      if(auth()->user()->role != 'gerant')
        abort(403);

      $compte->update([
        'date_fermeture' => $compte->date_fermeture ? null : date("Y-m-d H:i:s"),
        'visa_fermeture' => $compte->date_fermeture ? null : auth()->user()->name,
      ]);

      return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Compte  $compte
     * @return \Illuminate\Http\Response
     */
    public function edit(Compte $compte)
    {
      return view('contents.comptes.edit', [
        'clients' => Client::all(),
        'compte' => $compte,
      ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Compte  $compte
     * @return \Illuminate\Http\Response
     */
    public function update(CompteEditRequest $request, Compte $compte)
    {
      $input = $request->validated();
      $continue = $input['continue'] ?? route('show-compte', ['compte' => $compte->id]);
      unset($input['continue']);
      $compte->update($input);

      return response()->json([
        'success' => true,
        'message' => "Le Compte a bien été mis à jour.",
        // 'input' => $input,
        'redirect' => $continue,
      ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Compte  $compte
     * @return \Illuminate\Http\Response
     */
    public function destroy(Compte $compte)
    {
      $compte->delete();

      return redirect()->route('list-comptes');
    }



    public function agios()
    {
      $comptes = Compte::all();
      foreach ($comptes as $compte) {
        $compte->decrement("solde", 1000);
        Operation::create([
          "numero" => "011".str_pad(Operation::count()+1, 7, mt_rand(1000, 10000), STR_PAD_LEFT),
          "date" => date("Y-m-d H:i:s"),
          "montant" => 1000,
          "visa" => auth()->user()->name,
          "observation" => "Prélèvement mensuel AGIOS",
          "agence_id" => 1,
          "mode_operation_id" => 3,
          "type_operation_id" => 3,
          "compte_id" => $compte->id,
          "compte_dest_id" => null,
        ]);
      }

      return redirect()->back();
    }
}
