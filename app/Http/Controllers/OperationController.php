<?php

namespace App\Http\Controllers;

use App\Models\Operation;
use App\Models\Client;
use App\Models\Compte;
use App\Models\Agence;
use App\Models\ModeOperation;
use App\Models\TypeOperation;
use Illuminate\Http\Request;
use App\Http\Requests\DepotMakeRequest;
use App\Http\Requests\RetraitMakeRequest;
use App\Http\Requests\TransferMakeRequest;
use Yajra\DataTables\Facades\DataTables as FacadesDataTables;
use Carbon\Carbon;

class OperationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      if ($request->ajax()) {
        $model = Operation::query();
        return FacadesDataTables::eloquent($model->with(['agence', 'compte', 'compte.client', 'type', 'mode', 'compte_dest']))
        ->filter(function ($query) {
          if (request()->has('query.q')) {
            $query->where('numero', 'like', "%" . request('query.q') . "%");
          }
          if(request()->has('query.agence') && request("query.agence") != "all") {
            $query->where('agence_id', request('query.agence'));
          }
          if(request()->has('query.type') && request("query.type") != "all") {
            $query->where('type_operation_id', request('query.type'));
          }
          if(request()->has('query.mode') && request("query.mode") != "all") {
            $query->where('mode_operation_id', request('query.mode'));
          }
          if(request()->has('sort.field') && request()->has('sort.sort') && in_array(request('sort.field'), ["DT_RowIndex", "numero", "date", "montant", "agence", "type_operation", "compte", "created_at"]) && in_array(request('sort.sort'), ['asc', 'desc'])) {
            $cols = [
              "DT_RowIndex" => "id",
              "numero" => "numero",
              "date" => "date",
              "montant" => "montant",
              "agence" => "agence_id",
              "type_operation" => "type_operation_id",
              "compte" => "compte_id",
              "created_at" => "created_at",
            ];
            $query->orderBy($cols[request("sort.field")], request("sort.sort"));
          } else {
            $query->orderByDesc('id');
          }
        }, true)
        ->addIndexColumn()
        ->editColumn('photo', function (Operation $o) {
          return $o->compte->client->photo_path;
        })
        ->editColumn('created_at', function (Operation $o) {
          return Carbon::parse($o->created_at)->locale('fr')->isoFormat("DD MMM YYYY, HH:mm");
        })
        ->addColumn('nom_complet', function (Operation $o) {
          return $o->compte->client->nom_complet;
        })
        ->addColumn('action', function ($row) {
          return 'Edit/Delete';
        })
        ->toJson();
      }

      return view("contents.operations.list", [
        'agences' => Agence::all(),
        'modes_operation' => ModeOperation::all(),
        'types_operation' => TypeOperation::all(),
        'count' => Operation::count(),
      ]);
    }

    public function jsonComptes(Request $request)
    {
      return response()->json(
        Compte::where('numero', 'like', "%" . request('q') . "%")
          ->with('client')
          ->orderBy('numero')
          ->paginate(30),
      );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function deposit()
    {
      return view("contents.operations.deposit", [
        'agences' => Agence::all(),
        'modes_operation' => ModeOperation::all(),
      ]);
    }

    public function storeDeposit(DepotMakeRequest $request)
    {
      $input = $request->validated();
      $input['type_operation_id'] = 1;
      $input['date'] = date("Y-m-d H:i:s");
      $input['visa'] = auth()->user()->nom_complet;

      $op = Operation::create($input);
      $op->compte->increment('solde', $op->montant);

      return response()->json([
        'success' => true,
        'message' => "Dépôt effectué avec succès !",
        // 'input' => $input,
        'redirect' => route('list-operations'),
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function withdraw()
    {
      return view("contents.operations.withdraw", [
        'agences' => Agence::all(),
      ]);
    }

    public function storeWithdraw(RetraitMakeRequest $request)
    {
      $input = $request->validated();
      $input['type_operation_id'] = 2;
      $input['mode_operation_id'] = 1;
      $input['montant'] = abs($input['montant']);
      $input['date'] = date("Y-m-d H:i:s");
      $input['visa'] = auth()->user()->nom_complet;

      $compte = Compte::find($input['compte_id']);

      if($compte->solde >= $input['montant']) {
        $op = Operation::create($input);
        $compte->decrement('solde', $op->montant);

        return response()->json([
          'success' => true,
          'message' => "Retrait effectué avec succès !",
          // 'input' => $input,
          'redirect' => route('list-operations'),
        ]);
      } else {
        return response()->json([
          'success' => false,
          'message' => "Compte ".$compte->numero. " - Solde insuffisant.",
          // 'input' => $input,
        ]);
      }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function transfer()
    {
      return view("contents.operations.transfer", [
        'agences' => Agence::all(),
        'modes_operation' => ModeOperation::all(),
      ]);
    }

    public function storeTransfer(TransferMakeRequest $request)
    {
      $input = $request->validated();

      $input['type_operation_id'] = 3;
      $input['mode_operation_id'] = 3;
      $input['montant'] = abs($input['montant']);
      $input['date'] = date("Y-m-d H:i:s");
      $input['visa'] = auth()->user()->nom_complet;

      $compte_src = Compte::find($input['compte_id']);
      $compte_dest = Compte::find($input['compte_dest_id']);

      if($compte_src->solde >= $input['montant']) {
        $op = Operation::create($input);
        $compte_src->decrement('solde', $op->montant);
        $compte_dest->increment('solde', $op->montant);

        return response()->json([
          'success' => true,
          'message' => "Transfert effectué avec succès !",
          // 'input' => $input,
          'redirect' => route('list-operations'),
        ]);
      } else {
        return response()->json([
          'success' => false,
          'message' => "Compte ".$compte_src->numero. " - Solde insuffisant.",
          // 'input' => $input,
        ]);
      }

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Operation  $operation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Operation $operation)
    {
        //
    }
}
