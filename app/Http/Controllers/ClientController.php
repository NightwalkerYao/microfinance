<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Sexe;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\ClientCreateRequest;
use Yajra\DataTables\Facades\DataTables as FacadesDataTables;
use Carbon\Carbon;

class ClientController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index(Request $request)
  {
    if ($request->ajax()) {
      $model = Client::query();
      return FacadesDataTables::eloquent($model->with(['sexe']))
      ->filter(function ($query) {
        if (request()->has('query.q')) {
          $query->where('nom', 'like', "%" . request('query.q') . "%")
          ->orWhere('prenom', 'like', "%" . request('query.q') . "%")
          ->orWhere('telephone', 'like', "%" . request('query.q') . "%");
        }
        if(request()->has('query.sexe') && request("query.sexe") != "all") {
          $query->where('sexe_id', request('query.sexe'));
        }
        if(request()->has('sort.field') && request()->has('sort.sort') && in_array(request('sort.field'), ["DT_RowIndex", "nom_complet", "nom", "prenom", "telephone", "sexe", "created_at"]) && in_array(request('sort.sort'), ['asc', 'desc'])) {
          $cols = [
            "DT_RowIndex" => "id",
            "nom_complet" => "prenom",
            "cni" => "cni",
            "nom" => "nom",
            "prenom" => "prenom",
            "telephone" => "telephone",
            "sexe" => "sexe_id",
            "created_at" => "created_at",
          ];
          $query->orderBy($cols[request("sort.field")], request("sort.sort"));
        } else {
          $query->orderByDesc('id');
        }
      }, true)
      ->addIndexColumn()
      ->editColumn('photo', function (Client $c) {
        return asset(empty($c->photo) ? '/assets/media/users/blank.png' : str_replace('public/', '', 'storage/'.$c->photo));
      })
      ->editColumn('created_at', function (Client $c) {
        return Carbon::parse($c->created_at)->locale('fr')->isoFormat("DD MMM YYYY, HH:mm");
      })
      ->addColumn('nom_complet', function (Client $c) {
        return $c->prenom ." ". $c->nom;
      })
      ->addColumn('action', function ($row) {
        return 'Edit/Delete';
      })
      ->toJson();
    }

    return view("contents.clients.list", [
      'count' => Client::count(),
      'sexes' => Sexe::all(),
    ]);
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    return view('contents.clients.create', [
      'sexes' => Sexe::all(),
    ]);
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(ClientCreateRequest $request)
  {
    $input = $request->validated();
    if($request->hasFile('signature_file') && $request->file('signature_file')->isValid())
    $input['signature'] = str_replace('public/', '', $request->file('signature_file')->store('public/uploads'));
    if($request->hasFile('photo_file') && $request->file('photo_file')->isValid())
    $input['photo'] = str_replace('public/', '', $request->file('photo_file')->store('public/uploads'));

    $client = Client::create($input);

    copy(storage_path("app/public/".$client->photo), storage_path("app/public/photos/".basename($client->photo)));

    User::create([
      'name' => $client->nom_complet,
      'matricule' => mt_rand(1000000, 99999999).(range("A", "Z")[mt_rand(0,25)]),
      'photo' => basename($client->photo),
      'email' => \Str::slug($client->nom_complet, '.').$client->id."@microfinance.com",
      'password' => \Hash::make(123456),
      'role' => "client",
      'active' => true,
      'client_id' => $client->id,
    ]);

    return response()->json([
      'success' => true,
      'message' => "Le Client a bien été enregistré.",
      // 'input' => $input,
      'redirect' => route('list-clients'),
    ]);
  }

  /**
  * Display the specified resource.
  *
  * @param  \App\Models\Client  $client
  * @return \Illuminate\Http\Response
  */
  public function show(Client $client)
  {
    return redirect()->route('show-compte', ['compte' => $client->comptes->first()->id]);
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  \App\Models\Client  $client
  * @return \Illuminate\Http\Response
  */
  public function edit(Client $client)
  {
    return view('contents.clients.edit', [
      'sexes' => Sexe::all(),
      'client' => $client,
    ]);
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  \App\Models\Client  $client
  * @return \Illuminate\Http\Response
  */
  public function update(ClientCreateRequest $request, Client $client)
  {
    $input = $request->validated();
    if(!empty($input['photo_file_remove']))
      $input['photo'] = null;
    if(!empty($input['signature_file_remove']))
      $input['signature'] = null;
    if($request->hasFile('signature_file') && $request->file('signature_file')->isValid())
      $input['signature'] = str_replace('public/', '', $request->file('signature_file')->store('public/uploads'));
    if($request->hasFile('photo_file') && $request->file('photo_file')->isValid())
      $input['photo'] = str_replace('public/', '', $request->file('photo_file')->store('public/uploads'));

    $continue = $input['continue'] ?? route('show-client', ['client' => $client->id]);
    unset($input['photo_file_remove']);
    unset($input['photo_file']);
    unset($input['signature_file']);
    unset($input['signature_file_remove']);
    unset($input['continue']);

    $client->update($input);

    return response()->json([
      'success' => true,
      'message' => "Le Client a bien été mis à jour.",
      // 'input' => $input,
      'redirect' => $continue,
    ]);
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  \App\Models\Client  $client
  * @return \Illuminate\Http\Response
  */
  public function destroy(Client $client)
  {
    $client->delete();

    return redirect()->route('list-clients');
  }
}
