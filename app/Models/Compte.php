<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Compte extends Model
{
    use HasFactory;

    protected $fillable = [
      "numero",
      "date_ouverture",
      "visa_ouverture",
      "date_fermeture",
      "visa_fermeture",
      "observation",
      "solde",
      "client_id",
    ];


    protected $casts = [
      "date_ouverture" => "datetime",
      "date_fermeture" => "datetime",
      "solde" => "double",
    ];


    public function client()
    {
      return $this->belongsTo(Client::class);
    }

    public function operations()
    {
      return $this->hasMany(Operation::class, 'compte_id');
    }

    public function calculate_total($type)
    {
      $t = 0;
      foreach ($this->operations->where('type_operation_id', $type) as $op) {
        $t += $op->montant;
      }
      return $t;
    }
}
