<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'matricule',
        'photo',
        'email',
        'password',
        'role',
        'active',
        'client_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'active' => "boolean",
    ];


    public function getPhotoPathAttribute()
    {
      return asset(empty($this->photo) ? '/assets/media/users/blank.png' : str_replace('public/', '', 'storage/photos/'.$this->photo));
    }

    public function getClientAttribute()
    {
      if($this->client_id)
        return $this->belongsTo(Client::class);
      else
        return Client::first(); // Remove this on Prod
    }
}
